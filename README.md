- Universidad de San Carlos de Guatemala
- Facultad de Ingeniería
- Escuela de Ingeniería en Ciencias y Sistemas
- Organización de Lenguajes y Compiladores 2
- Primer semestre 2020


- Arit Software es un entorno y lenguaje de programación con un enfoque en el análisis estadístico, el cual  está formado por un conjunto de herramientas muy flexibles que pueden ampliarse fácilmente  mediante paquetes, librerías o definiendo nuestras propias funciones. Este software también cuenta con un entorno de desarrollo integrado (IDE) que proporciona servicios integrales para facilitarle al programador el desarrollo de aplicaciones.

- El proyecto es una aplicación de escritorio utilizando Java.
- Se reaiza 2 veces la gramática para el mismo lenguaje, pero utilizando diferentes analizadores, además de agregar las acciones correspondientes a cada gramática para el correcto funcionamiento de estas.
- Se realiza una gramática ascendente utilizando flex y cup.
- Se debe realiza una gramática descendente utilizando JavaCC.
