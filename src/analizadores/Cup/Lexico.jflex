package analizadores.Cup;
import java_cup.runtime.Symbol;
import java.util.ArrayList;


%% 
%class Lexico
%cupsym TablaSimbolos
%function next_token
%type java_cup.runtime.Symbol
%public 
%line 
%column 
%char 
%cup 
%unicode
%ignorecase

%init{ 
    yyline = 1; 
    yycolumn = 1; 

 /* The %init directive allows us to introduce code in the class constructor. We are using it for initializing our variables */
this.string = new StringBuffer();
this.tokensList = new ArrayList();
this.tokensListErrores = new ArrayList();

%init}

%{
public StringBuffer string;
public static ArrayList tokensList;
public static ArrayList<String> tokensListErrores;;
%}


indentificador= [\.]?[:jletter:]+[:jletterdigit:]*
numerosEnteros=/*[-]?*/[:digit:]+
numerosDecimales=/*[-]?*/[:digit:]+("."[:digit:]+)?
cadena = [\"]([^\"]|(\\\"))*[\"]
//cadena = [\"]([^\"\n]|(\\\"))*[\"]



/* comments */
comentario = ("#".*\r\n)|("#".*\n)|("#".*\r) 
comentarioMultilinea = "#*""#"*([^*#]|[^*]"#"|"*"[^/])*"*"*"*#"

LineTerminator = \r|\n|\r\n
WhiteSpace     = {LineTerminator} | [ \t\f]+ 

%state STRING
 
%%

{cadena} { this.tokensList.add("CADENA: "+yytext());
return new Symbol(TablaSimbolos.ERCADENA,yyline,yycolumn, (yytext()).substring(1,yytext().length()-1)); } 


/*PALABRAS RESERVADAS*/

//<YYINITIAL>{

"null" {this.tokensList.add("NULL: "+yytext());
return new Symbol(TablaSimbolos.TNULL, yyline,yycolumn, new String(yytext())); }


"true" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TTRUE, yyline,yycolumn, new String(yytext()).toLowerCase()); }
"false" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TFALSE, yyline,yycolumn, new String(yytext()).toLowerCase()); }
"do" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TDO, yyline,yycolumn, new String(yytext())); }
"while" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TWHILE, yyline,yycolumn, new String(yytext())); }
"print" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TPRINT, yyline,yycolumn, new String(yytext())); }
"println" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TPRINTLN, yyline,yycolumn, new String(yytext())); }
"if" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TIF, yyline,yycolumn, new String(yytext())); }
"else" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TELSE, yyline,yycolumn, new String(yytext())); }
"for" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TFOR, yyline,yycolumn, new String(yytext())); }
"in" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TIN, yyline,yycolumn, new String(yytext())); }
"case" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TCASE, yyline,yycolumn, new String(yytext())); }
"switch" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TSWITCH, yyline,yycolumn, new String(yytext())); }
"default" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TDEFAULT, yyline,yycolumn, new String(yytext())); }
"break" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TBREAK, yyline,yycolumn, new String(yytext())); }
"return" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TRETURN, yyline,yycolumn, new String(yytext())); }
"continue" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TCONTINUE, yyline,yycolumn, new String(yytext())); }
"printtabla" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TPRINTTABLE, yyline,yycolumn, new String(yytext())); }
//"matrix" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TMATRIX, yyline,yycolumn, new String(yytext())); }
"function" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.TFUNCTION, yyline,yycolumn, new String(yytext())); }

//}



"+" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.MAS,yyline,yycolumn, yytext());} 
"-" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.MENOS,yyline,yycolumn, yytext());} 
"*" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.POR,yyline,yycolumn, yytext());} 
"/" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.DIV,yyline,yycolumn, yytext());} 
"%%" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.MODULO,yyline,yycolumn, yytext());} 
"&" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.AND,yyline,yycolumn, yytext());} 
"|" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.OR,yyline,yycolumn, yytext());} 
"^" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.POTENCIA,yyline,yycolumn, yytext());} 
">" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.MAYQUE,yyline,yycolumn, yytext());} 
"<" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.MENQUE,yyline,yycolumn, yytext());} 
">=" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.MAYIGUALQUE,yyline,yycolumn, yytext());} 
"<=" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.MENIGUALQUE,yyline,yycolumn, yytext());} 
"--" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.DECREMENTO,yyline,yycolumn, yytext());} 
"++" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.INCREMENTO,yyline,yycolumn, yytext());} 
"==" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.IGUALQUE,yyline,yycolumn, yytext());} 
"!=" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.DIFQUE,yyline,yycolumn, yytext());} 
"!" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.NOT,yyline,yycolumn, yytext());}
"+=" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.MASIGUAL,yyline,yycolumn, yytext());} 
"*=" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.PORIGUAL,yyline,yycolumn, yytext());} 
"-=" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.MENOSIGUAL,yyline,yycolumn, yytext());}  
"/=" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.DIVIGUAL,yyline,yycolumn, yytext());} 
"=" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.IGUAL,yyline,yycolumn, yytext());} 
"(" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.PARIZQ,yyline,yycolumn, yytext());} 
")" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.PARDER,yyline,yycolumn, yytext());} 
"[" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.CORIZQ,yyline,yycolumn, yytext());} 
"]" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.CORDER,yyline,yycolumn, yytext());} 
"{" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.LLAVEIZQ,yyline,yycolumn, yytext());} 
"}" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.LLAVEDER,yyline,yycolumn, yytext());} 
"," {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.COMA,yyline,yycolumn, yytext());} 
";" {this.tokensList.add("PTCOMA: "+yytext()); return new Symbol(TablaSimbolos.PTCOMA,yyline,yycolumn, yytext());} 
":" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.DOSPUNTOS,yyline,yycolumn, yytext());} 
"." {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.PUNTO,yyline,yycolumn, yytext());} 
"?" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.INTERROGACION,yyline,yycolumn, yytext());} 
//"=>" {this.tokensList.add("Token: "+yytext()); return new Symbol(TablaSimbolos.FLECHITA,yyline,yycolumn, yytext());} 




   /* \"                             { yybegin(STRING); string.setLength(0); }



    <STRING> {
      \"                             { yybegin(YYINITIAL); 
                                       this.tokensList.add("STRING: "+yytext());
                                       return new Symbol(TablaSimbolos.ERCADENA, yyline,yycolumn,this.string.toString()); }
      [^\n\r\"\\]+                   { this.string.append( new String(yytext())); }
      \\t                            { this.string.append('\t'); }
      \\n                            { this.string.append('\n'); }

      \\r                            { this.string.append('\r'); }
      \\\"                           { this.string.append('\"'); }
      \\                             { this.string.append('\\'); }
    }*/



{indentificador} { this.tokensList.add("ID: "+yytext());
return new Symbol(TablaSimbolos.ERID, yyline,yycolumn, new String(yytext()));}

{numerosEnteros} { this.tokensList.add("Token: "+yytext());
return new Symbol(TablaSimbolos.ERENTERO, yyline,yycolumn, Integer.parseInt(yytext()));}

{numerosDecimales} { this.tokensList.add("Token: "+yytext());
return new Symbol(TablaSimbolos.ERDECIMAL, yyline,yycolumn, Double.parseDouble(yytext()));}

{comentario} {}//{ this.tokensList.add("Token: "+yytext());
//return new Symbol(TablaSimbolos.ercomentario, yyline,yycolumn, new String(yytext()));}

{comentarioMultilinea} {}//{ this.tokensList.add("Token: "+yytext());
//return new Symbol(TablaSimbolos.ercomentariomultilinea, yyline,yycolumn, new String(yytext()));}



/* CUALQUIER OTRO */
{WhiteSpace}            { /* se ignoran  */}
.                       {this.tokensListErrores.add("Linea: "+yyline+", Columna: "+yycolumn+ ", Error Léxico: "+yytext());}