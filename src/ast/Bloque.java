/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast;

import ast.entorno.Entorno;
import ast.entorno.Funcion;
import ast.expresion.Expresion;
import ast.instrucciones.Declaracion;
import ast.instrucciones.Instruccion;
import java.util.LinkedList;

/**
 *
 * @author Kenia
 */
public class Bloque implements Instruccion {

    public LinkedList<NodoAst> instrucciones;
    int linea, columna;
    public Entorno entorno = null;

    public Bloque(LinkedList<NodoAst> instrucciones, int linea, int columna) {
        this.instrucciones = instrucciones;
        this.linea = linea;
        this.columna = columna;
    }

    @Override
    public Object ejecutar(Entorno e) {
        entorno = new Entorno(e);

        for (NodoAst nodo : instrucciones) {

            if (nodo instanceof Funcion) {
                Funcion funcion = (Funcion) nodo;
                e.agregar(funcion.id, funcion);
            } else if (nodo instanceof Declaracion) {
                Declaracion declaracion = (Declaracion) nodo;
                declaracion.ejecutar(e);
            } else if (nodo instanceof Instruccion) {
                Object result = ((Instruccion) nodo).ejecutar(entorno);
                if (result != null) {
                    return result;
                }
            } else if (nodo instanceof Expresion) {
                Expresion expr = (Expresion) nodo;
                Object result = expr.getValorImplicito(entorno);

              if (result != null) {
                    return result;
                }
            }

        }
        return null;

    }

    @Override
    public int linea() {
        return this.linea;
    }

    @Override
    public int columna() {
        return this.columna;
    }

}
