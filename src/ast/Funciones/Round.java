/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.Funciones;

import ast.Singleton;
import ast.Vectores.NodoVector;
import ast.entorno.Entorno;
import ast.entorno.Tipo;
import ast.expresion.Expresion;
import ast.instrucciones.Instruccion;
import java.text.DecimalFormat;
import java.util.LinkedList;

/**
 *
 * @author Kenia
 */
public class Round implements Instruccion {

    public LinkedList<Expresion> valoresParametros;
    int linea, columna;

    public Round(LinkedList<Expresion> valoresParametros, int linea, int columna) {
        this.valoresParametros = valoresParametros;
        this.linea = linea;
        this.columna = columna;
    }

    @Override
    public Object ejecutar(Entorno ent) {

        if (valoresParametros.size() == 1) {
            Expresion valor = valoresParametros.get(0);
            Tipo tipoData = valor.getTipo(ent);
            NodoVector vector = new NodoVector();
            double d = 0.436789436287643872;
            DecimalFormat decimalFormat = new DecimalFormat("0");

            if (tipoData.rol == Tipo.Rol.VECTOR && tipoData.tp == Tipo.tipo.NUMERIC) {
                vector = (NodoVector) valor.getValorImplicito(ent);
                if (vector.size() == 1) {
                    Double val = Double.valueOf(vector.get(0).toString());
                    String outpoutString = decimalFormat.format(val);
                    int res = Integer.parseInt(outpoutString);
                    return res;
                } else {
                    Singleton.getInstance().addError("Tamaño de vector mayor a 1 para la función TRUNK ", linea, columna);
                }
            } else if (tipoData.rol == Tipo.Rol.PRIMITIVO && tipoData.tp == Tipo.tipo.NUMERIC) {
                Double val = Double.valueOf(valor.getValorImplicito(ent).toString());
                String outpoutString = decimalFormat.format(val);
                int res = Integer.parseInt(outpoutString);
                return res;
            } else {
                Singleton.getInstance().addError("Tipo de dato incorrecto para la función TRUNK ", linea, columna);
                return null;
            }

        }

        return null;
    }

    @Override
    public int linea() {
        return this.linea;
    }

    @Override
    public int columna() {
        return this.columna;
    }

}
