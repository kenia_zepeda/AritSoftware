/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.Funciones;

import ast.Singleton;
import ast.Vectores.NodoVector;
import ast.entorno.Entorno;
import ast.entorno.Tipo;
import ast.expresion.Expresion;
import ast.instrucciones.Instruccion;
import java.util.LinkedList;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

/**
 *
 * @author Kenia
 */
public class Mean implements Instruccion {

    public LinkedList<Expresion> valoresParametros;
    int linea, columna;

    public Mean(LinkedList<Expresion> valoresParametros, int linea, int columna) {
        this.valoresParametros = valoresParametros;
        this.linea = linea;
        this.columna = columna;
    }

    @Override
    public Object ejecutar(Entorno ent) {

        if (valoresParametros.size() == 1) {
            Expresion valores = valoresParametros.get(0);
            Tipo tipoData = valores.getTipo(ent);
            NodoVector vectorValores = new NodoVector(tipoData);

            if (tipoData.rol == Tipo.Rol.VECTOR && (tipoData.tp == Tipo.tipo.INTEGER || tipoData.tp == Tipo.tipo.NUMERIC)) {
                vectorValores = (NodoVector) valores.getValorImplicito(ent);
            } else if (tipoData.rol == Tipo.Rol.PRIMITIVO && (tipoData.tp == Tipo.tipo.INTEGER || tipoData.tp == Tipo.tipo.NUMERIC)) {
                vectorValores.add(valores.getValorImplicito(ent));
            } else {
                Singleton.getInstance().addError("Tipo de dato incorrecto para los VALORES de la GRÁFICA DE PIE ", linea, columna);
                return null;
            }

            DescriptiveStatistics descriptiveStatistics = new DescriptiveStatistics();
            for (int i = 0; i < vectorValores.size(); i++) {	// El segundo índice recorre las columnas.
                Double val = Double.parseDouble(vectorValores.get(i).toString());
                descriptiveStatistics.addValue(val);
            }
            double mean = descriptiveStatistics.getMean();
            return mean;
        } else if (valoresParametros.size() == 2) {
            Expresion valores = valoresParametros.get(0);
            Tipo tipoData = valores.getTipo(ent);
            Expresion trim = valoresParametros.get(1);
            Tipo tipoTrim = trim.getTipo(ent);
            NodoVector vectorValores = new NodoVector(tipoData);

            if (tipoData.rol == Tipo.Rol.VECTOR && (tipoData.tp == Tipo.tipo.INTEGER || tipoData.tp == Tipo.tipo.NUMERIC)) {
                vectorValores = (NodoVector) valores.getValorImplicito(ent);
            } else if (tipoData.rol == Tipo.Rol.PRIMITIVO && (tipoData.tp == Tipo.tipo.INTEGER || tipoData.tp == Tipo.tipo.NUMERIC)) {
                vectorValores.add(valores.getValorImplicito(ent));
            } else {
                Singleton.getInstance().addError("Tipo de dato incorrecto para los VALORES de la GRÁFICA DE PIE ", linea, columna);
                return null;
            }

            if (tipoTrim.tp == Tipo.tipo.INTEGER) {
                int valTrim = Integer.parseInt(trim.getValorImplicito(ent).toString());
                DescriptiveStatistics descriptiveStatistics = new DescriptiveStatistics();
                for (int i = 0; i < vectorValores.size(); i++) {	// El segundo índice recorre las columnas.
                    Double val = Double.parseDouble(vectorValores.get(i).toString());
                    if (val >= valTrim) {
                        descriptiveStatistics.addValue(val);
                    }
                }
                double mean = descriptiveStatistics.getMean();
                return mean;

            } else {
                Singleton.getInstance().addError("Tipo de dato incorrecto para los TRIM de la función MEDIAN ", linea, columna);
                return null;
            }
        } else {
            Singleton.getInstance().addError("Catidad incorrecta de parámetros para la función MEDIAN ", linea, columna);

        }

        return null;
    }

    @Override
    public int linea() {
        return this.linea;
    }

    @Override
    public int columna() {
        return this.columna;
    }

}
