/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.Funciones;

import ast.Listas.NodoLista;
import ast.Singleton;
import ast.Vectores.NodoVector;
import ast.entorno.Entorno;
import ast.entorno.Tipo;
import ast.expresion.Expresion;
import ast.instrucciones.Instruccion;
import ast.matrix.NodoMatrix;
import java.util.LinkedList;

/**
 *
 * @author Kenia
 */
public class Length implements Instruccion {

    public LinkedList<Expresion> valoresParametros;
    int linea, columna;

    public Length(LinkedList<Expresion> valoresParametros, int linea, int columna) {
        this.valoresParametros = valoresParametros;
        this.linea = linea;
        this.columna = columna;
    }

    @Override
    public Object ejecutar(Entorno ent) {

        if (valoresParametros.size() == 1) {
            Expresion valor = valoresParametros.get(0);
            Tipo tipoData = valor.getTipo(ent);

            if (tipoData.rol == Tipo.Rol.VECTOR) {
                NodoVector vector = (NodoVector) valor.getValorImplicito(ent);
                return vector.size();
            } else if (tipoData.rol == Tipo.Rol.LISTA) {
                NodoLista lista = (NodoLista) valor.getValorImplicito(ent);
                return lista.size();
            } else if (tipoData.rol == Tipo.Rol.MATRIZ) {
                NodoMatrix matrix = (NodoMatrix) valor.getValorImplicito(ent);
                return matrix.ncols * matrix.nrows;
            } else if (tipoData.rol == Tipo.Rol.PRIMITIVO) {
                return 1;
            } else {
                Singleton.getInstance().addError("Tipo de dato incorrecto para la función LENGTH ", linea, columna);
                return null;
            }

        }

        return null;

    }

    @Override
    public int linea() {
        return this.linea;
    }

    @Override
    public int columna() {
        return this.columna;
    }

}
