/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.Funciones;

import ast.Singleton;
import ast.Vectores.NodoVector;
import ast.entorno.Entorno;
import ast.entorno.Tipo;
import ast.expresion.Expresion;
import ast.instrucciones.Instruccion;
import java.util.LinkedList;

/**
 *
 * @author Kenia
 */
public class StringLength implements Instruccion {

    public LinkedList<Expresion> valoresParametros;
    int linea, columna;

    public StringLength(LinkedList<Expresion> valoresParametros, int linea, int columna) {
        this.valoresParametros = valoresParametros;
        this.linea = linea;
        this.columna = columna;
    }

    @Override
    public Object ejecutar(Entorno ent) {

        if (valoresParametros.size() == 1) {
            Expresion valor = valoresParametros.get(0);
            Tipo tipoData = valor.getTipo(ent);
            NodoVector vector = new NodoVector();

            if (tipoData.rol == Tipo.Rol.VECTOR && tipoData.tp == Tipo.tipo.STRING) {
                vector = (NodoVector) valor.getValorImplicito(ent);
                if (vector.size() == 1) {
                    String val = vector.get(0).toString();
                    return val.length();
                } else {
                    Singleton.getInstance().addError("Tamaño de vector mayor a 1 para la función LENGTH ", linea, columna);
                }
            } else if (tipoData.rol == Tipo.Rol.PRIMITIVO && tipoData.tp == Tipo.tipo.STRING) {
                String val = valor.getValorImplicito(ent).toString();
                return val.length();
            } else {
                Singleton.getInstance().addError("Tipo de dato incorrecto para la función LENGTH ", linea, columna);
                return null;
            }

        }

        return null;
    }

    @Override
    public int linea() {
        return this.linea;
    }

    @Override
    public int columna() {
        return this.columna;
    }


}
