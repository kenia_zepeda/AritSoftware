/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.Funciones;

import ast.Singleton;
import ast.entorno.Entorno;
import ast.entorno.Tipo;
import ast.expresion.Expresion;
import ast.instrucciones.Instruccion;
import ast.matrix.NodoMatrix;
import java.util.LinkedList;

/**
 *
 * @author Kenia
 */
public class Nrow implements Instruccion {

    public LinkedList<Expresion> valoresParametros;
    int linea, columna;

    public Nrow(LinkedList<Expresion> valoresParametros, int linea, int columna) {
        this.valoresParametros = valoresParametros;
        this.linea = linea;
        this.columna = columna;
    }

    @Override
    public Object ejecutar(Entorno ent) {

        if (valoresParametros.size() == 1) {
            Expresion valor = valoresParametros.get(0);
            Tipo tipoData = valor.getTipo(ent);

            NodoMatrix matrizValores = new NodoMatrix();

            if (tipoData.rol == Tipo.Rol.MATRIZ) {
                matrizValores = (NodoMatrix) valor.getValorImplicito(ent);
                return matrizValores.nrows;
            } else {
                Singleton.getInstance().addError("Tipo de dato incorrecto para la función NROW", linea, columna);

            }

        }

        return null;

    }

    @Override
    public int linea() {
        return this.linea;
    }

    @Override
    public int columna() {
        return this.columna;
    }

}
