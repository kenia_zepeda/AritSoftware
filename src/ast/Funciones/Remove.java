/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.Funciones;

import ast.Singleton;
import ast.Vectores.NodoVector;
import ast.entorno.Entorno;
import ast.entorno.Tipo;
import ast.expresion.Expresion;
import ast.instrucciones.Instruccion;
import java.util.LinkedList;

/**
 *
 * @author Kenia
 */
public class Remove implements Instruccion {

    public LinkedList<Expresion> valoresParametros;
    int linea, columna;

    public Remove(LinkedList<Expresion> valoresParametros, int linea, int columna) {
        this.valoresParametros = valoresParametros;
        this.linea = linea;
        this.columna = columna;
    }

    @Override
    public Object ejecutar(Entorno ent) {

        if (valoresParametros.size() == 2) {
            Expresion valor1 = valoresParametros.get(0);
            Tipo tipoData1 = valor1.getTipo(ent);
            Expresion valor2 = valoresParametros.get(1);
            Tipo tipoData2 = valor2.getTipo(ent);
            NodoVector vector1 = new NodoVector();
            NodoVector vector2 = new NodoVector();
            String val1 = "";
            String val2 = "";

            if (tipoData1.rol == Tipo.Rol.VECTOR && tipoData1.tp == Tipo.tipo.STRING) {
                vector1 = (NodoVector) valor1.getValorImplicito(ent);
                if (vector1.size() == 1) {
                    val1 = vector1.get(0).toString();
                } else {
                    Singleton.getInstance().addError("Tamaño de vector mayor a 1 para la función REMOVE ", linea, columna);
                    return null;
                }
            } else if (tipoData1.rol == Tipo.Rol.PRIMITIVO && tipoData1.tp == Tipo.tipo.STRING) {
                val1 = valor1.getValorImplicito(ent).toString();
            } else {
                Singleton.getInstance().addError("Tipo de dato incorrecto para la función REMOVE ", linea, columna);
                return null;
            }

            if (tipoData2.rol == Tipo.Rol.VECTOR && tipoData2.tp == Tipo.tipo.STRING) {
                vector2 = (NodoVector) valor2.getValorImplicito(ent);
                if (vector2.size() == 1) {
                    val2 = vector2.get(0).toString();
                } else {
                    Singleton.getInstance().addError("Tamaño de vector mayor a 1 para la función REMOVE ", linea, columna);
                    return null;
                }
            } else if (tipoData2.rol == Tipo.Rol.PRIMITIVO && tipoData2.tp == Tipo.tipo.STRING) {
                val2 = valor2.getValorImplicito(ent).toString();
            } else {
                Singleton.getInstance().addError("Tipo de dato incorrecto para la función REMOVE ", linea, columna);
                return null;
            }

            String resultado = val1.replaceAll(val1, val2);
            return resultado;

        }

        return null;
    }


    @Override
    public int linea() {
        return this.linea;
    }

    @Override
    public int columna() {
        return this.columna;
    }


}
