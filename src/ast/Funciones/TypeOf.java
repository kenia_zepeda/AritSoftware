/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.Funciones;

import ast.entorno.Entorno;
import ast.entorno.Tipo;
import ast.expresion.Expresion;
import ast.instrucciones.Instruccion;
import java.util.LinkedList;

/**
 *
 * @author Kenia
 */
public class TypeOf implements Instruccion {

    public LinkedList<Expresion> valoresParametros;
    int linea, columna;

    public TypeOf(LinkedList<Expresion> valoresParametros, int linea, int columna) {
        this.valoresParametros = valoresParametros;
        this.linea = linea;
        this.columna = columna;
    }

    public TypeOf(Expresion valor, int linea, int columna) {
    }

    @Override
    public Object ejecutar(Entorno e) {

        if (valoresParametros.size() == 1) {
            Expresion valor = valoresParametros.get(0);
            Tipo tipo = valor.getTipo(e);

            if (tipo != null) {
                return tipo.tp.toString().toLowerCase();
            }
        }

        return "desconocido";
    }

    @Override
    public int linea() {
        return this.linea;
    }

    @Override
    public int columna() {
        return this.columna;
    }

}
