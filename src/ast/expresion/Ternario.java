/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.expresion;

import ast.entorno.Entorno;
import ast.entorno.Tipo;
import java.io.Serializable;

/**
 *
 * @author Kenia
 */
public class Ternario implements Expresion, Serializable {

    private Expresion condicion;
    private Expresion instruccionTrue;
    private Expresion instruccionElse;
    public Tipo tipo;
    int linea,columna;

    public Ternario(Expresion condicion, Expresion instruccionTrue, Expresion instruccionElse, int linea, int columna) {
        this.condicion = condicion;
        this.instruccionTrue = instruccionTrue;
        this.instruccionElse = instruccionElse;
        this.linea = linea;
        this.columna = columna;
    }

    @Override
    public Object getValorImplicito(Entorno ent) {
        Boolean valorCondicion = ((condicion == null) ? false : (Boolean) condicion.getValorImplicito(ent));
        if (valorCondicion) {

            Object r;
            r = instruccionTrue.getValorImplicito(ent);
            this.tipo = instruccionTrue.getTipo(ent);
            if (r != null) {
                return r;
            }
        } else {
            Object r;
            r = instruccionElse.getValorImplicito(ent);
            this.tipo = instruccionElse.getTipo(ent);
            if (r != null) {
                return r;
            }
        }
        return null;
    }

    @Override
    public Tipo  getTipo(Entorno ent) {

        Boolean valorCondicion = ((condicion == null) ? false : (Boolean) condicion.getValorImplicito(ent));
        if (valorCondicion) {
            this.tipo = instruccionTrue.getTipo(ent);
            if (tipo != null) {
                return tipo;
            }
        } else {
            this.tipo = instruccionElse.getTipo(ent);
            if (tipo != null) {
                return tipo;
            }
        }
        return null;
    }

    @Override
    public int linea() {
        return this.linea;
    }

    @Override
    public int columna() {
        return this.columna;
    }
}
