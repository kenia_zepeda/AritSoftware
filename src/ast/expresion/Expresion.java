/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.expresion;

import ast.NodoAst;
import ast.entorno.Entorno;
import ast.entorno.Tipo;


/**
 *
 * @author Kenia
 */
public interface Expresion extends NodoAst {

    Object getValorImplicito(Entorno e);
    
    Tipo getTipo(Entorno e);
        
}
