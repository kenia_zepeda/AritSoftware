/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.expresion;

import ast.Funciones.Mean;
import ast.Funciones.Median;
import ast.Funciones.Mode;
import ast.Funciones.TypeOf;
import ast.Graficas.Barplot;
import ast.Graficas.HistogramaPlot;
import ast.Graficas.LineasPlot;
import ast.Graficas.Pie;
import ast.Listas.FuncionList;
import ast.Listas.NodoLista;
import ast.Singleton;
import ast.Vectores.Concatenar;
import ast.Vectores.NodoVector;
import ast.entorno.Entorno;
import ast.entorno.Funcion;
import ast.entorno.Simbolo;
import ast.entorno.Tipo;
import ast.instrucciones.Declaracion;
import ast.matrix.FuncionMatrix;
import ast.matrix.NodoMatrix;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.LinkedList;

/**
 *
 * @author Kenia
 */
public class LlamadaFuncion implements Expresion {

    public String identificador;
    public LinkedList<Expresion> valoresParametros;
    public Tipo tipo;
    public Object valor = null;
    int linea, columna;

    public LlamadaFuncion(String identificador, LinkedList<Expresion> valoresParametros, int linea, int columna) {
        this.identificador = identificador;
        this.valoresParametros = valoresParametros;
        this.linea = linea;
        this.columna = columna;
    }

    public LlamadaFuncion(String identificador, int linea, int columna) {
        this.identificador = identificador;
        this.valoresParametros = new LinkedList<Expresion>();
        this.linea = linea;
        this.columna = columna;
    }

    @Override
    public Object getValorImplicito(Entorno ent) {

        if (identificador.toLowerCase().equals("c")) {
            Concatenar con = new Concatenar(valoresParametros, linea, columna);
            valor = con.getValorImplicito(ent);
            return valor;
        } else if (identificador.toLowerCase().equals("list")) {
            FuncionList con = new FuncionList(valoresParametros, linea, columna);
            valor = con.getValorImplicito(ent);
            return valor;
        } else if (identificador.toLowerCase().equals("matrix")) {
            return verificarParametrosMatrix(ent);
        } else if (identificador.toLowerCase().equals("pie")) {
            return verificarParametrosPie(ent);
        } else if (identificador.toLowerCase().equals("barplot")) {
            return verificarParametrosBar(ent);
        } else if (identificador.toLowerCase().equals("hist")) {
            return verificarParametrosHist(ent);
        } else if (identificador.toLowerCase().equals("plot")) {
            return verificarParametrosLineas(ent);
        } else if (identificador.toLowerCase().equals("typeof")) {
            TypeOf funcion = new TypeOf(valoresParametros, linea, columna);
            return funcion.ejecutar(ent);
        } else if (identificador.toLowerCase().equals("length")) {
            return verificarParametrosLength(ent);
        } else if (identificador.toLowerCase().equals("nrow")) {
            return verificarParametrosNrow(ent);
        } else if (identificador.toLowerCase().equals("ncol")) {
            return verificarParametrosNcol(ent);
        } else if (identificador.toLowerCase().equals("stringlength")) {
            return verificarParametrosStringLength(ent);
        } else if (identificador.toLowerCase().equals("remove")) {
            return verificarParametrosRemove(ent);
        } else if (identificador.toLowerCase().equals("tolowercase")) {
            return verificarParametrosLowercase(ent);
        } else if (identificador.toLowerCase().equals("touppercase")) {
            return verificarParametrosUppercase(ent);
        } else if (identificador.toLowerCase().equals("trunk")) {
            return verificarParametrosTrunk(ent);
        } else if (identificador.toLowerCase().equals("round")) {
            return verificarParametrosRound(ent);
        } else if (identificador.toLowerCase().equals("median")) {
            Median funcion = new Median(valoresParametros, linea, columna);
            return funcion.ejecutar(ent);
        } else if (identificador.toLowerCase().equals("mean")) {
            Mean funcion = new Mean(valoresParametros, linea, columna);
            return funcion.ejecutar(ent);
        } else if (identificador.toLowerCase().equals("mode")) {
            Mode funcion = new Mode(valoresParametros, linea, columna);
            return funcion.ejecutar(ent);
        }

        if (ent.existe(identificador)) {

            Funcion funcion = (Funcion) ent.getSimbolo(identificador, 0, 0);
            Entorno local = new Entorno(ent);
            if (verificarParametros(valoresParametros, funcion.listaParametros, local)) {
                //aqui va el retorno 
                valor = funcion.ejecutar(local);
                return valor;
            }

        } else {
            Singleton.getInstance().addError("La funcion " + identificador + " no ha sido declarada", linea, columna);
        }
        return null;
    }

    public boolean verificarParametros(LinkedList<Expresion> valores, LinkedList<Simbolo> parametros, Entorno ent) {

        if (valores.size() == parametros.size()) {
            Simbolo parametroSimbolo;
            String parametroId;
            Expresion parametroExpresion;

            Expresion valorExpresion;

            for (int i = 0; i < parametros.size(); i++) {

                parametroSimbolo = (Simbolo) parametros.get(i);
                parametroId = parametroSimbolo.getId();
                parametroExpresion = (Expresion) parametroSimbolo.getValor();

                valorExpresion = valores.get(i);

                /**
                 * Cuando se haga una llamada se utilizará la pablara reservado
                 * default, que nos dará a entender que queremos utilizar el
                 * valor por defecto
                 */
                if (valorExpresion instanceof Default) {
                    if (parametroExpresion != null) {
                        Tipo tipo = parametroExpresion.getTipo(ent);
                        ent.agregar(parametroId, new Simbolo(tipo, parametroId, parametroExpresion.getValorImplicito(ent)));

                        /* if (ent.existeEnActual(parametroId)) {
                            ent.getSimbolo(parametroId, linea, columna).setValor(parametroExpresion.getValorImplicito(ent));
                            ent.getSimbolo(parametroId, linea, columna).setTipo(tipo);
                        } else {
                            ent.agregar(parametroId, new Simbolo(tipo, parametroId, parametroExpresion.getValorImplicito(ent)));
                        }*/
                    } else {
                        Singleton.getInstance().addError("Llamada a: " + identificador + ", el parámetro: " + parametroId + " no tiene valor por defecto.", linea, columna);
                        return false;
                    }
                } else {
                    Tipo tipo = valorExpresion.getTipo(ent);
                    ent.agregar(parametroId, new Simbolo(tipo, parametroId, valorExpresion.getValorImplicito(ent)));

                    /*if (ent.existe(parametroId)) {
                        ent.getSimbolo(parametroId, linea, columna).setValor(valorExpresion.getValorImplicito(ent));
                        ent.getSimbolo(parametroId, linea, columna).setTipo(tipo);
                    } else {
                        ent.agregar(parametroId, new Simbolo(tipo, parametroId, valorExpresion.getValorImplicito(ent)));
                    }*/

 /* Declaracion declarar = new Declaracion(parametroId, valorExpresion, linea, columna);
                    declarar.ejecutar(ent);*/
                }

            }

            return true;
        } else {
            Singleton.getInstance().addError("Llamada a: " + identificador + ", La cantidad de parámetros no coincide ", linea, columna);
        }

        return false;

    }

    public Object verificarParametrosMatrix(Entorno ent) {

        if (valoresParametros.size() == 3) {
            Expresion data = valoresParametros.get(0);
            Expresion nrow = valoresParametros.get(1);
            Expresion ncol = valoresParametros.get(2);
            Tipo tipoData = data.getTipo(ent);
            Tipo tiponrow = nrow.getTipo(ent);
            Tipo tiponcol = ncol.getTipo(ent);
            tipo = tipoData;
            NodoVector vector = new NodoVector(tipoData);
            int filas = 0;
            int columnas = 0;

            if (tipoData.rol == Tipo.Rol.VECTOR) {
                vector = (NodoVector) data.getValorImplicito(ent);
            } else if (tipoData.rol == Tipo.Rol.PRIMITIVO) {
                vector.add(data.getValorImplicito(ent));
            } else {
                Singleton.getInstance().addError("Tipo de dato incorrecto para la DATA de la matrix ", linea, columna);
                return null;
            }

            if (tiponrow.tp == Tipo.tipo.INTEGER) {
                filas = (int) nrow.getValorImplicito(ent);
            } else {
                Singleton.getInstance().addError("Tipo de dato incorrecto para las FILAS de la matrix ", linea, columna);
                return null;
            }

            if (tiponcol.tp == Tipo.tipo.INTEGER) {
                columnas = (int) ncol.getValorImplicito(ent);
            } else {
                Singleton.getInstance().addError("Tipo de dato incorrecto para las COLUMNAS de la matrix ", linea, columna);
                return null;
            }

            FuncionMatrix con = new FuncionMatrix(vector, filas, columnas, linea, columna);
            valor = con.getValorImplicito(ent);
            return valor;

        }

        return null;
    }

    public Object verificarParametrosPie(Entorno ent) {

        if (valoresParametros.size() == 3) {
            Expresion valores = valoresParametros.get(0);
            Expresion etiquetas = valoresParametros.get(1);
            Expresion titulo = valoresParametros.get(2);
            Tipo tipoData = valores.getTipo(ent);
            Tipo tipoEtiquetas = etiquetas.getTipo(ent);
            Tipo Titulo = titulo.getTipo(ent);
            NodoVector vectorValores = new NodoVector();
            NodoVector vectorEtiquetas = new NodoVector();
            String titulovalor = "";

            if (tipoData.rol == Tipo.Rol.VECTOR && (tipoData.tp == Tipo.tipo.INTEGER || tipoData.tp == Tipo.tipo.NUMERIC)) {
                vectorValores = (NodoVector) valores.getValorImplicito(ent);
            } else if (tipoData.rol == Tipo.Rol.PRIMITIVO && (tipoData.tp == Tipo.tipo.INTEGER || tipoData.tp == Tipo.tipo.NUMERIC)) {
                vectorValores.add(valores.getValorImplicito(ent));
            } else {
                Singleton.getInstance().addError("Tipo de dato incorrecto para los VALORES de la GRÁFICA DE PIE ", linea, columna);
                return null;
            }

            if (tipoEtiquetas.rol == Tipo.Rol.VECTOR && tipoEtiquetas.tp == Tipo.tipo.STRING) {
                vectorEtiquetas = (NodoVector) etiquetas.getValorImplicito(ent);
            } else if (tipoEtiquetas.rol == Tipo.Rol.PRIMITIVO && tipoEtiquetas.tp == Tipo.tipo.STRING) {
                vectorEtiquetas.add(etiquetas.getValorImplicito(ent));
            } else {
                Singleton.getInstance().addError("Tipo de dato incorrecto para las ETIQUETAS de la GRÁFICA DE PIE ", linea, columna);
                return null;
            }

            if (Titulo.tp == Tipo.tipo.STRING) {
                titulovalor = (String) titulo.getValorImplicito(ent);
            } else {
                Singleton.getInstance().addError("Tipo de dato incorrecto para el Titulo de la GRÁFICA DE PIE ", linea, columna);
                return null;
            }

            Pie con = new Pie(vectorValores, vectorEtiquetas, titulovalor, linea, columna);
            valor = con.ejecutar(ent);
            return valor;

        }

        return null;
    }

    public Object verificarParametrosBar(Entorno ent) {

        if (valoresParametros.size() == 5) {
            Expresion valores = valoresParametros.get(0);
            Expresion tituloX = valoresParametros.get(1);
            Expresion tituloY = valoresParametros.get(2);
            Expresion titulo = valoresParametros.get(3);
            Expresion etiquetas = valoresParametros.get(4);
            Tipo tipoData = valores.getTipo(ent);
            Tipo tipoEtiquetas = etiquetas.getTipo(ent);
            Tipo tipoTitulo = titulo.getTipo(ent);
            Tipo tipoTituloX = tituloX.getTipo(ent);
            Tipo tipoTituloY = tituloY.getTipo(ent);
            NodoVector vectorValores = new NodoVector();
            NodoVector vectorEtiquetas = new NodoVector();
            String titulovalor = "";
            String tituloXvalor = "";
            String tituloYvalor = "";

            if (tipoData.rol == Tipo.Rol.VECTOR && (tipoData.tp == Tipo.tipo.INTEGER || tipoData.tp == Tipo.tipo.NUMERIC)) {
                vectorValores = (NodoVector) valores.getValorImplicito(ent);
            } else if (tipoData.rol == Tipo.Rol.PRIMITIVO && (tipoData.tp == Tipo.tipo.INTEGER || tipoData.tp == Tipo.tipo.NUMERIC)) {
                vectorValores.add(valores.getValorImplicito(ent));
            } else {
                Singleton.getInstance().addError("Tipo de dato incorrecto para los VALORES de la GRÁFICA DE BARRAS ", linea, columna);
                return null;
            }

            if (tipoEtiquetas.rol == Tipo.Rol.VECTOR && tipoEtiquetas.tp == Tipo.tipo.STRING) {
                vectorEtiquetas = (NodoVector) etiquetas.getValorImplicito(ent);
            } else if (tipoEtiquetas.rol == Tipo.Rol.PRIMITIVO && tipoEtiquetas.tp == Tipo.tipo.STRING) {
                vectorEtiquetas.add(etiquetas.getValorImplicito(ent));
            } else {
                Singleton.getInstance().addError("Tipo de dato incorrecto para las ETIQUETAS de la GRÁFICA DE BARRAS ", linea, columna);
                return null;
            }

            if (tipoTitulo.rol == Tipo.Rol.PRIMITIVO) {
                titulovalor = titulo.getValorImplicito(ent).toString();
            } else {
                Singleton.getInstance().addError("Tipo de dato incorrecto para el Titulo de la GRÁFICA DE BARRAS ", linea, columna);
                return null;
            }

            if (tipoTituloX.rol == Tipo.Rol.PRIMITIVO) {
                tituloXvalor = tituloX.getValorImplicito(ent).toString();
            } else {
                Singleton.getInstance().addError("Tipo de dato incorrecto para el TituloX de la GRÁFICA DE BARRAS ", linea, columna);
                return null;
            }

            if (tipoTituloY.rol == Tipo.Rol.PRIMITIVO) {
                tituloYvalor = tituloY.getValorImplicito(ent).toString();
            } else {
                Singleton.getInstance().addError("Tipo de dato incorrecto para el TituloY de la GRÁFICA DE BARRAS ", linea, columna);
                return null;
            }

            Barplot con = new Barplot(vectorValores, vectorEtiquetas, titulovalor, tituloXvalor, tituloYvalor, linea, columna);
            valor = con.ejecutar(ent);
            return valor;

        }

        return null;
    }

    public Object verificarParametrosHist(Entorno ent) {

        if (valoresParametros.size() == 3) {
            Expresion valores = valoresParametros.get(0);
            Expresion tituloX = valoresParametros.get(1);
            Expresion titulo = valoresParametros.get(2);
            Tipo tipoData = valores.getTipo(ent);
            Tipo tipoTitulo = titulo.getTipo(ent);
            Tipo tipoTituloX = tituloX.getTipo(ent);
            NodoVector vectorValores = new NodoVector();
            String titulovalor = "";
            String tituloXvalor = "";

            if (tipoData.rol == Tipo.Rol.VECTOR && (tipoData.tp == Tipo.tipo.INTEGER || tipoData.tp == Tipo.tipo.NUMERIC)) {
                vectorValores = (NodoVector) valores.getValorImplicito(ent);
            } else if (tipoData.rol == Tipo.Rol.PRIMITIVO && (tipoData.tp == Tipo.tipo.INTEGER || tipoData.tp == Tipo.tipo.NUMERIC)) {
                vectorValores.add(valores.getValorImplicito(ent));
            } else {
                Singleton.getInstance().addError("Tipo de dato incorrecto para los VALORES de la GRÁFICA DE BARRAS ", linea, columna);
                return null;
            }

            if (tipoTitulo.rol == Tipo.Rol.PRIMITIVO) {
                titulovalor = titulo.getValorImplicito(ent).toString();
            } else {
                Singleton.getInstance().addError("Tipo de dato incorrecto para el Titulo de la GRÁFICA DE BARRAS ", linea, columna);
                return null;
            }

            if (tipoTituloX.rol == Tipo.Rol.PRIMITIVO) {
                tituloXvalor = tituloX.getValorImplicito(ent).toString();
            } else {
                Singleton.getInstance().addError("Tipo de dato incorrecto para el TituloX de la GRÁFICA DE BARRAS ", linea, columna);
                return null;
            }

            HistogramaPlot con = new HistogramaPlot(vectorValores, tituloXvalor, titulovalor, linea, columna);
            valor = con.ejecutar(ent);
            return valor;
        }

        return null;
    }

    public Object verificarParametrosLineas(Entorno ent) {

        if (valoresParametros.size() == 5) {
            Expresion valores = valoresParametros.get(0);
            Expresion parametroY = valoresParametros.get(1);
            Expresion tituloX = valoresParametros.get(2);
            Expresion tituloY = valoresParametros.get(3);
            Expresion parametroX = valoresParametros.get(4);
            Tipo tipoData = valores.getTipo(ent);
            Tipo tipoParametroY = parametroY.getTipo(ent);
            Tipo tipoparametroX = parametroX.getTipo(ent);
            Tipo tipoTituloX = tituloX.getTipo(ent);
            Tipo tipoTituloY = tituloY.getTipo(ent);
            NodoVector vectorValores = new NodoVector();
            NodoMatrix matrizValores = new NodoMatrix();
            String tipoValor = "";
            String titulovalor = "";
            String tituloXvalor = "";
            String tituloYvalor = "";

            if (tipoData.rol == Tipo.Rol.VECTOR && (tipoData.tp == Tipo.tipo.INTEGER || tipoData.tp == Tipo.tipo.NUMERIC)) {
                vectorValores = (NodoVector) valores.getValorImplicito(ent);
            } else if (tipoData.rol == Tipo.Rol.MATRIZ && (tipoData.tp == Tipo.tipo.INTEGER || tipoData.tp == Tipo.tipo.NUMERIC)) {
                matrizValores = (NodoMatrix) valores.getValorImplicito(ent);
                Object[][] matrix = matrizValores.getMatrix();
                for (int i = 0; i < matrizValores.nrows; i++) {
                    // El primer índice recorre las filas.
                    for (int j = 0; j < matrizValores.ncols; j++) {	// El segundo índice recorre las columnas.
                        vectorValores.add(matrix[i][j]);
                    }
                }
            } else if (tipoData.rol == Tipo.Rol.PRIMITIVO && (tipoData.tp == Tipo.tipo.INTEGER || tipoData.tp == Tipo.tipo.NUMERIC)) {
                vectorValores.add(valores.getValorImplicito(ent));
            } else {
                Singleton.getInstance().addError("Tipo de dato incorrecto para los VALORES de la GRÁFICA DE BARRAS ", linea, columna);
                return null;
            }

            if (tipoParametroY.rol == Tipo.Rol.PRIMITIVO) {
                tipoValor = parametroY.getValorImplicito(ent).toString();
            } else {
                Singleton.getInstance().addError("Tipo de dato incorrecto para el Tipo de la GRÁFICA DE LÍNEAS ", linea, columna);
                return null;
            }

            if (tipoTituloX.rol == Tipo.Rol.PRIMITIVO) {
                tituloXvalor = tituloX.getValorImplicito(ent).toString();
            } else {
                Singleton.getInstance().addError("Tipo de dato incorrecto para el TituloX de la GRÁFICA DE LÍNEAS ", linea, columna);
                return null;
            }

            if (tipoTituloY.rol == Tipo.Rol.PRIMITIVO) {
                tituloYvalor = tituloY.getValorImplicito(ent).toString();
            } else {
                Singleton.getInstance().addError("Tipo de dato incorrecto para el TituloY de la GRÁFICA DE LÍNEAS ", linea, columna);
                return null;
            }

            if (tipoparametroX.tp == Tipo.tipo.STRING) {
                titulovalor = parametroX.getValorImplicito(ent).toString();
                LineasPlot con = new LineasPlot(vectorValores, tipoValor, tituloXvalor, tituloYvalor, titulovalor, linea, columna);
                valor = con.ejecutar(ent);
                return valor;
            } else if (tipoparametroX.rol == Tipo.Rol.VECTOR && (tipoparametroX.tp == Tipo.tipo.INTEGER || tipoparametroX.tp == Tipo.tipo.NUMERIC)) {
                NodoVector vector = (NodoVector) parametroX.getValorImplicito(ent);
                LineasPlot con = new LineasPlot(vectorValores, tipoValor, tituloXvalor, tituloYvalor, vector, linea, columna);
                valor = con.ejecutar(ent);
                return valor;
            } else {
                Singleton.getInstance().addError("Tipo de dato incorrecto para el Titulo de la GRÁFICA DE LÍNEAS ", linea, columna);
                return null;
            }

        }
        return null;
    }

    public Object verificarParametrosLength(Entorno ent) {

        if (valoresParametros.size() == 1) {
            Expresion valor = valoresParametros.get(0);
            Tipo tipoData = valor.getTipo(ent);

            if (tipoData.rol == Tipo.Rol.VECTOR) {
                NodoVector vector = (NodoVector) valor.getValorImplicito(ent);
                return vector.size();
            } else if (tipoData.rol == Tipo.Rol.LISTA) {
                NodoLista lista = (NodoLista) valor.getValorImplicito(ent);
                return lista.size();
            } else if (tipoData.rol == Tipo.Rol.MATRIZ) {
                NodoMatrix matrix = (NodoMatrix) valor.getValorImplicito(ent);
                return matrix.ncols * matrix.nrows;
            } else if (tipoData.rol == Tipo.Rol.PRIMITIVO) {
                return 1;
            } else {
                Singleton.getInstance().addError("Tipo de dato incorrecto para la función LENGTH ", linea, columna);
                return null;
            }

        }

        return null;
    }

    public Object verificarParametrosTypeof(Entorno ent) {

        if (valoresParametros.size() == 1) {
            Expresion valor = valoresParametros.get(0);
            TypeOf funcion = new TypeOf(valor, linea, columna);
            return funcion.ejecutar(ent);

        }

        return null;
    }

    public Object verificarParametrosNcol(Entorno ent) {

        if (valoresParametros.size() == 1) {
            Expresion valor = valoresParametros.get(0);
            Tipo tipoData = valor.getTipo(ent);

            NodoMatrix matrizValores = new NodoMatrix();

            if (tipoData.rol == Tipo.Rol.MATRIZ) {
                matrizValores = (NodoMatrix) valor.getValorImplicito(ent);
                return matrizValores.ncols;
            } else {
                Singleton.getInstance().addError("Tipo de dato incorrecto para la función NCOL", linea, columna);

            }

        }

        return null;
    }

    public Object verificarParametrosNrow(Entorno ent) {

        if (valoresParametros.size() == 1) {
            Expresion valor = valoresParametros.get(0);
            Tipo tipoData = valor.getTipo(ent);

            NodoMatrix matrizValores = new NodoMatrix();

            if (tipoData.rol == Tipo.Rol.MATRIZ) {
                matrizValores = (NodoMatrix) valor.getValorImplicito(ent);
                return matrizValores.nrows;
            } else {
                Singleton.getInstance().addError("Tipo de dato incorrecto para la función NROW", linea, columna);

            }

        }

        return null;
    }

    public Object verificarParametrosStringLength(Entorno ent) {

        if (valoresParametros.size() == 1) {
            Expresion valor = valoresParametros.get(0);
            Tipo tipoData = valor.getTipo(ent);
            NodoVector vector = new NodoVector();

            if (tipoData.rol == Tipo.Rol.VECTOR && tipoData.tp == Tipo.tipo.STRING) {
                vector = (NodoVector) valor.getValorImplicito(ent);
                if (vector.size() == 1) {
                    String val = vector.get(0).toString();
                    return val.length();
                } else {
                    Singleton.getInstance().addError("Tamaño de vector mayor a 1 para la función LENGTH ", linea, columna);
                }
            } else if (tipoData.rol == Tipo.Rol.PRIMITIVO && tipoData.tp == Tipo.tipo.STRING) {
                String val = valor.getValorImplicito(ent).toString();
                return val.length();
            } else {
                Singleton.getInstance().addError("Tipo de dato incorrecto para la función LENGTH ", linea, columna);
                return null;
            }

        }

        return null;
    }

    public Object verificarParametrosRemove(Entorno ent) {

        if (valoresParametros.size() == 2) {
            Expresion valor1 = valoresParametros.get(0);
            Tipo tipoData1 = valor1.getTipo(ent);
            Expresion valor2 = valoresParametros.get(1);
            Tipo tipoData2 = valor2.getTipo(ent);
            NodoVector vector1 = new NodoVector();
            NodoVector vector2 = new NodoVector();
            String val1 = "";
            String val2 = "";

            if (tipoData1.rol == Tipo.Rol.VECTOR && tipoData1.tp == Tipo.tipo.STRING) {
                vector1 = (NodoVector) valor1.getValorImplicito(ent);
                if (vector1.size() == 1) {
                    val1 = vector1.get(0).toString();
                } else {
                    Singleton.getInstance().addError("Tamaño de vector mayor a 1 para la función REMOVE ", linea, columna);
                    return null;
                }
            } else if (tipoData1.rol == Tipo.Rol.PRIMITIVO && tipoData1.tp == Tipo.tipo.STRING) {
                val1 = valor1.getValorImplicito(ent).toString();
            } else {
                Singleton.getInstance().addError("Tipo de dato incorrecto para la función REMOVE ", linea, columna);
                return null;
            }

            if (tipoData2.rol == Tipo.Rol.VECTOR && tipoData2.tp == Tipo.tipo.STRING) {
                vector2 = (NodoVector) valor2.getValorImplicito(ent);
                if (vector2.size() == 1) {
                    val2 = vector2.get(0).toString();
                } else {
                    Singleton.getInstance().addError("Tamaño de vector mayor a 1 para la función REMOVE ", linea, columna);
                    return null;
                }
            } else if (tipoData2.rol == Tipo.Rol.PRIMITIVO && tipoData2.tp == Tipo.tipo.STRING) {
                val2 = valor2.getValorImplicito(ent).toString();
            } else {
                Singleton.getInstance().addError("Tipo de dato incorrecto para la función REMOVE ", linea, columna);
                return null;
            }

            String resultado = val1.replaceAll(val1, val2);
            return resultado;

        }

        return null;
    }

    public Object verificarParametrosLowercase(Entorno ent) {

        if (valoresParametros.size() == 1) {
            Expresion valor = valoresParametros.get(0);
            Tipo tipoData = valor.getTipo(ent);
            NodoVector vector = new NodoVector();

            if (tipoData.rol == Tipo.Rol.VECTOR && tipoData.tp == Tipo.tipo.STRING) {
                vector = (NodoVector) valor.getValorImplicito(ent);
                if (vector.size() == 1) {
                    String val = vector.get(0).toString();
                    return val.toLowerCase();
                } else {
                    Singleton.getInstance().addError("Tamaño de vector mayor a 1 para la función TOLOWERCASE ", linea, columna);
                }
            } else if (tipoData.rol == Tipo.Rol.PRIMITIVO && tipoData.tp == Tipo.tipo.STRING) {
                String val = valor.getValorImplicito(ent).toString();
                return val.toLowerCase();
            } else {
                Singleton.getInstance().addError("Tipo de dato incorrecto para la función TOLOWERCASE ", linea, columna);
                return null;
            }

        }

        return null;
    }

    public Object verificarParametrosUppercase(Entorno ent) {

        if (valoresParametros.size() == 1) {
            Expresion valor = valoresParametros.get(0);
            Tipo tipoData = valor.getTipo(ent);
            NodoVector vector = new NodoVector();

            if (tipoData.rol == Tipo.Rol.VECTOR && tipoData.tp == Tipo.tipo.STRING) {
                vector = (NodoVector) valor.getValorImplicito(ent);
                if (vector.size() == 1) {
                    String val = vector.get(0).toString();
                    return val.toUpperCase();
                } else {
                    Singleton.getInstance().addError("Tamaño de vector mayor a 1 para la función TOUPPERCASE ", linea, columna);
                }
            } else if (tipoData.rol == Tipo.Rol.PRIMITIVO && tipoData.tp == Tipo.tipo.STRING) {
                String val = valor.getValorImplicito(ent).toString();
                return val.toUpperCase();
            } else {
                Singleton.getInstance().addError("Tipo de dato incorrecto para la función TOUPPERCASE ", linea, columna);
                return null;
            }

        }

        return null;
    }

    public Object verificarParametrosTrunk(Entorno ent) {

        if (valoresParametros.size() == 1) {
            Expresion valor = valoresParametros.get(0);
            Tipo tipoData = valor.getTipo(ent);
            NodoVector vector = new NodoVector();
            double d = 0.436789436287643872;
            DecimalFormat decimalFormat = new DecimalFormat("0");
            decimalFormat.setRoundingMode(RoundingMode.DOWN);

            if (tipoData.rol == Tipo.Rol.VECTOR && tipoData.tp == Tipo.tipo.NUMERIC) {
                vector = (NodoVector) valor.getValorImplicito(ent);
                if (vector.size() == 1) {
                    Double val = Double.valueOf(vector.get(0).toString());
                    String outpoutString = decimalFormat.format(val);
                    int res = Integer.parseInt(outpoutString);
                    return res;
                } else {
                    Singleton.getInstance().addError("Tamaño de vector mayor a 1 para la función TRUNK ", linea, columna);
                }
            } else if (tipoData.rol == Tipo.Rol.PRIMITIVO && tipoData.tp == Tipo.tipo.NUMERIC) {
                Double val = Double.valueOf(valor.getValorImplicito(ent).toString());
                String outpoutString = decimalFormat.format(val);
                int res = Integer.parseInt(outpoutString);
                return res;
            } else {
                Singleton.getInstance().addError("Tipo de dato incorrecto para la función TRUNK ", linea, columna);
                return null;
            }

        }

        return null;
    }

    public Object verificarParametrosRound(Entorno ent) {

        if (valoresParametros.size() == 1) {
            Expresion valor = valoresParametros.get(0);
            Tipo tipoData = valor.getTipo(ent);
            NodoVector vector = new NodoVector();
            double d = 0.436789436287643872;
            DecimalFormat decimalFormat = new DecimalFormat("0");

            if (tipoData.rol == Tipo.Rol.VECTOR && tipoData.tp == Tipo.tipo.NUMERIC) {
                vector = (NodoVector) valor.getValorImplicito(ent);
                if (vector.size() == 1) {
                    Double val = Double.valueOf(vector.get(0).toString());
                    String outpoutString = decimalFormat.format(val);
                    int res = Integer.parseInt(outpoutString);
                    return res;
                } else {
                    Singleton.getInstance().addError("Tamaño de vector mayor a 1 para la función TRUNK ", linea, columna);
                }
            } else if (tipoData.rol == Tipo.Rol.PRIMITIVO && tipoData.tp == Tipo.tipo.NUMERIC) {
                Double val = Double.valueOf(valor.getValorImplicito(ent).toString());
                String outpoutString = decimalFormat.format(val);
                int res = Integer.parseInt(outpoutString);
                return res;
            } else {
                Singleton.getInstance().addError("Tipo de dato incorrecto para la función TRUNK ", linea, columna);
                return null;
            }

        }

        return null;
    }

    @Override
    public Tipo getTipo(Entorno e
    ) {

        if (valor == null) {
            valor = this.getValorImplicito(e);
        }

        if (valor == null) {
            return new Tipo(Tipo.Rol.DESCONOCIDO, Tipo.tipo.DESCONOCIDO);

        }

        if (valor instanceof Boolean) {
            return new Tipo(Tipo.Rol.PRIMITIVO, Tipo.tipo.BOOLEAN);
        } else if (valor instanceof String) {
            return new Tipo(Tipo.Rol.PRIMITIVO, Tipo.tipo.STRING);
        } else if (valor instanceof Integer) {
            return new Tipo(Tipo.Rol.PRIMITIVO, Tipo.tipo.INTEGER);
        } else if (valor instanceof Double) {
            return new Tipo(Tipo.Rol.PRIMITIVO, Tipo.tipo.NUMERIC);
        } else if (valor instanceof NodoVector) {
            NodoVector vector = (NodoVector) valor;
            return new Tipo(Tipo.Rol.VECTOR, vector.tipo.tp);
        } else if (valor instanceof NodoLista) {
            return new Tipo(Tipo.Rol.LISTA, Tipo.tipo.LISTA);
        } else if (valor instanceof NodoMatrix) {
            NodoMatrix matrix = (NodoMatrix) valor;
            return new Tipo(Tipo.Rol.MATRIZ, matrix.tipo.tp);
        } else {
            return new Tipo(Tipo.Rol.DESCONOCIDO, Tipo.tipo.DESCONOCIDO);
        }
    }

    @Override
    public int linea() {
        return this.linea;
    }

    @Override
    public int columna() {
        return this.columna;
    }

}
