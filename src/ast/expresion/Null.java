/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.expresion;

import ast.entorno.Entorno;
import ast.entorno.Tipo;

/**
 *
 * @author Kenia
 */
public class Null implements Expresion {

    public Object valor;
    public Tipo tipo;
    public int linea, columna;

    public Null(Tipo tipo, Object valor, int linea, int columna) {
        this.valor = valor;
        this.tipo = tipo;
        this.linea = linea;
        this.columna = columna;
    }

    public Null() {
    }

    @Override
    public Object getValorImplicito(Entorno e) {
        return "0";
    }

    @Override
    public Tipo getTipo(Entorno e) {
        return new Tipo(Tipo.Rol.NULL, Tipo.tipo.NULL);
    }

    @Override
    public int linea() {
        return this.linea;
    }

    @Override
    public int columna() {
        return this.columna;
    }
}
