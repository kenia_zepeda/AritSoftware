/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.expresion;

import ast.Singleton;
import ast.entorno.Entorno;
import ast.entorno.Simbolo;
import ast.entorno.Tipo;

/**
 *
 * @author Kenia
 */
public class Identificador implements Expresion {

    public String nombre;
    public int linea, columna;

    public Identificador(String nombre, int linea, int columna) {
        this.nombre = nombre;
        this.linea = linea;
        this.columna = columna;
    }

    @Override
    public Tipo getTipo(Entorno e) {
        Simbolo sim = e.getSimbolo(nombre, linea, columna);

        if (sim != null) {
            return sim.getTipo();
        }

        /*   for (Entorno e = ent; e != null; e = e.getPadre()) {
            if (e.getTabla().containsKey(nombre)) {
                Simbolo encontrado = e.getTabla().get(nombre);
                return encontrado.getTipo();
            }
        }*/
        Singleton.getInstance().addError("El simbolo \"" + nombre + "\" no ha sido declarado en el entorno actual ni en alguno externo", linea, columna);
        return new Tipo(Tipo.Rol.DESCONOCIDO, Tipo.tipo.DESCONOCIDO);
    }

    @Override
    public Object getValorImplicito(Entorno e) {
        Simbolo sim = e.getSimbolo(nombre, linea, columna);

        if (sim != null) {
            return sim.getValor();
        }

        // Singleton.getInstance().addError("El simbolo \"" + nombre + "\" no ha sido declarado en el entorno actual ni en alguno externo", e.idClase, linea, columna);
        return null;

    }

    @Override
    public int linea() {
        return this.linea;
    }

    @Override
    public int columna() {
        return this.columna;
    }

}
