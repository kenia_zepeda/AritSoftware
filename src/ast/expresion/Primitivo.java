/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.expresion;

import ast.Vectores.NodoVector;
import ast.entorno.Entorno;
import ast.entorno.Tipo;
import java.util.ArrayList;

/**
 *
 * @author Kenia
 */
public class Primitivo implements Expresion {

    public Object valor;
    public Tipo tipo;
    public int linea, columna;

    public Primitivo(Tipo tipo, Object valor, int linea, int columna) {
        this.valor = valor;
        this.tipo = tipo;
        this.linea = linea;
        this.columna = columna;
    }

    public Primitivo(Tipo tipo, Object valor) {
        this.valor = valor;
        this.tipo = tipo;
    }

    public ArrayList<Object> getVector() {
        NodoVector vector_sim = new NodoVector(tipo);
        vector_sim.add(valor);
        return vector_sim;
    }

    @Override
    public Object getValorImplicito(Entorno e) {
        return this.valor;
    }

    public Object getThis(Entorno e) {
        return this;
    }

    @Override
    public Tipo getTipo(Entorno e) {
        return this.tipo;
    }

    @Override
    public int linea() {
        return this.linea;
    }

    @Override
    public int columna() {
        return this.columna;
    }

}
