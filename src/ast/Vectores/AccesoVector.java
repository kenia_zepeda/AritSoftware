/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.Vectores;

import ast.Singleton;
import ast.Singleton;
import ast.entorno.Entorno;
import ast.entorno.Simbolo;
import ast.entorno.Tipo;
import ast.expresion.Expresion;
import java.util.ArrayList;

/**
 *
 * @author Kenia
 */
public class AccesoVector implements Expresion {

    public String id;
    public Expresion posicion;
    public int linea, columna;

    public AccesoVector(String id, Expresion posicion, int linea, int columna) {
        this.id = id;
        this.posicion = posicion;
        this.linea = linea;
        this.columna = columna;
    }

    @Override
    public Object getValorImplicito(Entorno e) {
        Simbolo sim = e.getSimbolo(id, linea, columna);
        Object resultado = posicion.getValorImplicito(e);
        int pos;

        if (sim != null) {

            if (resultado instanceof Integer) {
                pos = (int) resultado - 1;
            } else {
                Singleton.getInstance().addError("La posición del vector no es correcta", linea, columna);
                return null;
            }

            if (null == sim.rol) {
                Singleton.getInstance().addError("La variable " + id + " no es un vector.", linea, columna);
                return null;
            } else {
                switch (sim.rol) {
                    case VECTOR:
                        ArrayList<Object> vector = (ArrayList<Object>) sim.valor;
                        Object valor = null;
                        if (pos < vector.size() && pos >= 0) {
                            valor = vector.get(pos);
                            return valor;
                        } else {
                            Singleton.getInstance().addError("La posición del vector no es correcta", linea, columna);

                        }
                        break;
                    case PRIMITIVO:
                        if (pos == 1) {
                            return sim.valor;

                        } else {
                            Singleton.getInstance().addError("La posición del vector no es correcta", linea, columna);
                        }
                        break;
                    default:
                        Singleton.getInstance().addError("La variable " + id + " no es un vector.", linea, columna);
                        return null;
                }
            }

        }
        return null;

    }

    @Override
    public Tipo getTipo(Entorno e) {
        Simbolo sim = e.getSimbolo(id, linea, columna);
        if (sim != null) {
            return new Tipo(Tipo.Rol.PRIMITIVO, sim.getTipo().tp);
        }
        return new Tipo(Tipo.Rol.DESCONOCIDO, Tipo.tipo.DESCONOCIDO);
    }

    @Override
    public int linea() {
        return this.linea;
    }

    @Override
    public int columna() {
        return this.columna;
    }

}
