/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.Vectores;

import ast.Singleton;
import ast.entorno.Entorno;
import ast.entorno.Simbolo;
import ast.entorno.Tipo;
import ast.expresion.Expresion;
import ast.instrucciones.Instruccion;
import java.util.ArrayList;

/**
 *
 * @author Kenia
 */
public class ModificarVector implements Instruccion {

    public String id;
    public Expresion posicion;
    public Expresion valor;
    public int linea, columna;

    public ModificarVector(String id, Expresion posicion, Expresion valor, int linea, int columna) {
        this.id = id;
        this.posicion = posicion;
        this.valor = valor;
        this.linea = linea;
        this.columna = columna;
    }

    @Override
    public Object ejecutar(Entorno e) {
        /**
         * Obtemos el tipo del "valor nuevo", para posteriormente verificar si
         * es una Función C o una valor primitivo
         */
        Tipo valorNuevoTipo = valor.getTipo(e);
        Object valorNuevo = null;

        if (null == valorNuevoTipo.rol) {
            return null;
        } else {
            switch (valorNuevoTipo.rol) {
                /**
                 * Si se intenta agregar o modificar un valor utilizando acceso
                 * por índice con una expresión función c, se debe verificar que
                 * la cantidad de parámetros sea 1, caso contrario reportar el
                 * error.
                 */
                case VECTOR:
                    ArrayList<Object> vector_exp = new ArrayList<>();
                    vector_exp = (ArrayList<Object>) valor.getValorImplicito(e);
                    if (vector_exp.size() == 1) {
                        valorNuevo = vector_exp.get(0);
                    } else {
                        Singleton.getInstance().addError("La cantidad de parámentros de la función C es incorrecta, debe ser 1 parámetro.", linea, columna);
                        return null;
                    }
                    break;
                case PRIMITIVO:
                    valorNuevo = valor.getValorImplicito(e);
                    break;
                default:
                    Singleton.getInstance().addError("Error en el tipo de dato.", linea, columna);
                    return null;
            }
        }

        /**
         * Obtenemos el Símbolo de tipo vector que vamos a modificar de la tabla
         * de simbolos
         */
        Simbolo sim = e.getSimbolo(id, linea, columna);
        /**
         * Obtenemos el ArrayList del símbolo
         */
        ArrayList<Object> vector_sim = sim.getVector();
        if (vector_sim.isEmpty()) {
            Singleton.getInstance().addError("La variable no es un vector", linea, columna);
            return null;
        }
        /**
         * Obtenemos el valor implicito de la posición
         */
        Object posicion_valor = posicion.getValorImplicito(e);
        int pos;

        if (sim != null) {

            /**
             * Verificamos el tipo del Vector a modificar y del nuevo valor, si
             * agregamos un nuevo valor al vector de un tipo distinto, se debe
             * hacer el casteo correspondiente explicado en la función c, para
             * mantener el mismo tipo de elementos.
             */
            switch (sim.tipo.tp) {

                case STRING:
                    if (valorNuevoTipo.tp != Tipo.tipo.STRING) {
                        valorNuevo = valorNuevo.toString();
                    }
                    break;
                case NUMERIC:
                    if (valorNuevoTipo.tp == Tipo.tipo.STRING) {
                        valorNuevo = valorNuevo.toString().replace("\"", "");
                        if (valorNuevo instanceof Double) {
                            valorNuevo = Double.parseDouble((String) valorNuevo);
                        } else {
                            Singleton.getInstance().addError("Error en el tipo de dato.", linea, columna);
                        }
                    } else if (valorNuevoTipo.tp == Tipo.tipo.INTEGER) {
                        valorNuevo = Double.parseDouble((String) valorNuevo);
                    } else if (valorNuevoTipo.tp == Tipo.tipo.BOOLEAN) {
                        valorNuevo = Boolean.parseBoolean(valorNuevo.toString().toLowerCase()) ? 1.0 : 0.0;
                    }
                    break;
                case INTEGER:
                    if (valorNuevoTipo.tp == Tipo.tipo.STRING) {
                        valorNuevo = valorNuevo.toString().replace("\"", "");
                        if (valorNuevo instanceof Integer) {
                            valorNuevo = Integer.parseInt((String) valorNuevo);
                        } else {
                            Singleton.getInstance().addError("Error en el tipo de dato.", linea, columna);
                        }
                    } else if (valorNuevoTipo.tp == Tipo.tipo.NUMERIC) {
                        valorNuevo = Integer.parseInt((String) valorNuevo);
                    } else if (valorNuevoTipo.tp == Tipo.tipo.BOOLEAN) {
                        valorNuevo = Boolean.parseBoolean(valorNuevo.toString().toLowerCase()) ? 1 : 0;
                    }
                    break;
                case BOOLEAN:
                    if (valorNuevoTipo.tp == Tipo.tipo.STRING) {
                        if (valorNuevo.toString().toLowerCase().equals("true")) {
                            valorNuevo = true;
                        } else if (valorNuevo.toString().toLowerCase().equals("false")) {
                            valorNuevo = false;
                        }
                    } else if (valorNuevoTipo.tp == Tipo.tipo.NUMERIC) {
                        Double val = Double.parseDouble((String) valorNuevo);
                        if (1.0 == val) {
                            valorNuevo = true;
                        } else if (0.0 == val) {
                            valorNuevo = false;
                        }
                    } else if (valorNuevoTipo.tp == Tipo.tipo.INTEGER) {
                        int val = Integer.parseInt((String) valorNuevo);
                        if (1 == val) {
                            valorNuevo = true;
                        } else if (0 == val) {
                            valorNuevo = false;
                        }
                    }

                    break;
                default:
                    Singleton.getInstance().addError("Error en el tipo de dato.", linea, columna);
                    return null;

            }

            /**
             * Verificamos que el valor de la posición sea de tipo entero
             */
            if (posicion_valor instanceof Integer) {
                pos = (int) posicion_valor -1;
            } else {
                Singleton.getInstance().addError("La posición del vector no es correcta", linea, columna);
                return null;
            }

            if (pos < vector_sim.size()) {
                vector_sim.set(pos, valorNuevo);
                e.getSimbolo(id, linea, columna).setValor(vector_sim);
            } else {

                /**
                 * Si se agrega un nuevo valor en un índice mayor al tamaño del
                 * vector, las posiciones entre la última posición del vector y
                 * el nuevo índice deben tomar el valor por defecto del tipo de
                 * vector que se esté manejando.
                 */
                int diferencia = pos - vector_sim.size();
                switch (sim.tipo.tp) {
                    case STRING:
                        for (int i = 0; i < diferencia; i++) {
                            vector_sim.add("NULL");
                        }
                        vector_sim.add(valorNuevo);
                        break;
                    case NUMERIC:
                        for (int i = 0; i < diferencia; i++) {
                            vector_sim.add(0.0);
                        }
                        vector_sim.add(valorNuevo);
                        break;
                    case INTEGER:
                        for (int i = 0; i < diferencia; i++) {
                            vector_sim.add(0);
                        }
                        vector_sim.add(valorNuevo);
                        break;
                    case BOOLEAN:
                        for (int i = 0; i < diferencia; i++) {
                            vector_sim.add(false);
                        }
                        vector_sim.add(valorNuevo);
                        break;
                    default:
                        break;
                }

            }

        }

        e.getSimbolo(id, linea, columna).setTipo(new Tipo(Tipo.Rol.VECTOR, sim.tipo.tp));
        e.getSimbolo(id, linea, columna).setValor(vector_sim);

        return null;

    }

    @Override
    public int linea() {
        return this.linea;
    }

    @Override
    public int columna() {
        return this.columna;
    }

}
