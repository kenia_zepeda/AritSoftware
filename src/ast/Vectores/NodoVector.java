/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.Vectores;

import ast.entorno.Tipo;
import java.util.ArrayList;

/**
 *
 * @author Kenia
 */
public class NodoVector extends ArrayList<Object> {

    public Tipo tipo;

    public NodoVector() {
        this.tipo = new Tipo(Tipo.Rol.VECTOR, Tipo.tipo.DESCONOCIDO);

    }

    public NodoVector(Tipo tipo) {
        this.tipo = tipo;
    }

    public Tipo getTipo() {
        return tipo;
    }

    public void setTipo(Tipo tipo) {
        this.tipo = tipo;
    }
    
    

}
