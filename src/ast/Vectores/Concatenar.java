/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.Vectores;

import ast.Listas.NodoLista;
import ast.Listas.ObjetoLista;
import ast.Singleton;
import ast.Vectores.NodoVector;
import ast.entorno.Entorno;
import ast.entorno.Tipo;
import ast.expresion.Expresion;
import java.util.ArrayList;
import java.util.LinkedList;

/**
 *
 * @author Kenia
 */
public class Concatenar implements Expresion {

    public Tipo tipo;
    public LinkedList<Expresion> valores;
    public int linea, columna;

    boolean listaFlag = false;
    boolean vectorFlag = false;
    boolean stringFlag = false;
    boolean doubleFlag = false;
    boolean intFlag = false;
    boolean booleanFlag = false;

    public Concatenar(LinkedList<Expresion> Exps, int linea, int columna) {
        this.valores = Exps;
        this.linea = linea;
        this.columna = columna;
        this.tipo = new Tipo(Tipo.Rol.VECTOR, Tipo.tipo.DESCONOCIDO);
    }

    @Override
    public Object getValorImplicito(Entorno e) {
        return concatenar(e);
    }

    public Object concatenar(Entorno e) {
        NodoVector vector = new NodoVector();
        NodoLista lista = new NodoLista();
        Tipo tipo_exp = null;

        /**
         * El valor de retorno de la función c estará dado por los elementos que
         * este contenga, si son distintos tipos de dato se deberán castear los
         * elementos por prioridad de casteo: 1. Lista 2. String 3. Numeric 4.
         * Int
         */
        for (Expresion exp : valores) {

            tipo_exp = exp.getTipo(e);

            if (null != tipo_exp) {
                switch (tipo_exp.tp) {
                    case LISTA:
                        listaFlag = true;
                        break;
                    case STRING:
                        stringFlag = true;
                        break;
                    case NUMERIC:
                        doubleFlag = true;
                        break;
                    case INTEGER:
                        intFlag = true;
                        break;
                    case BOOLEAN:
                        booleanFlag = true;
                        break;
                    default:
                        Singleton.getInstance().addError("Error en el tipo de dato del vector", linea, columna);
                        return null;

                }
            }
        }

        /**
         * Si existe una función c, con distintos tipos de elementos y algún
         * elemento es una lista, el resto de los elementos pasaran a ser parte
         * de esa lista.
         */
        if (listaFlag) {
            for (Expresion exp : valores) {
                tipo_exp = exp.getTipo(e);

                if (null != tipo_exp) {
                    switch (tipo_exp.rol) {
                        case LISTA:
                            lista.addAll((ArrayList) exp.getValorImplicito(e));
                            break;
                        case VECTOR:
                            NodoVector vect = (NodoVector) exp.getValorImplicito(e);
                            for (Object object : vect) {
                                ObjetoLista objeto = new ObjetoLista(new Tipo(Tipo.Rol.PRIMITIVO, tipo_exp.tp), object);
                                lista.add(objeto);
                            }

                            //   lista.addAll((ArrayList) exp.getValorImplicito(e));
                            break;
                        case PRIMITIVO:
                            ObjetoLista objeto = new ObjetoLista(tipo_exp, exp.getValorImplicito(e));
                            lista.add(objeto);
                            break;
                    }
                }
            }
            return lista;
            /**
             * Si existen varios vectores, se formará un nuevo vector con los
             * elementos de todos los vectores y casteando a los tipos
             * correspondientes.
             */
            /**
             * Si los elementos tienen distintos tipos de datos se deberá ver
             * cuál es el que tiene más prioridad y aplicar el tipo de casteo
             * correspondiente. /** Si existen varios vectores, se formará un
             * nuevo vector con los elementos de todos los vectores y casteando
             * a los tipos correspondientes.
             */
            /**
             * Si los elementos tienen distintos tipos de datos se deberá ver
             * cuál es el que tiene más prioridad y aplicar el tipo de casteo
             * correspondiente.
             */
        } else if (stringFlag) {
            vector = new NodoVector(new Tipo(Tipo.Rol.VECTOR, Tipo.tipo.STRING));
            for (Expresion exp : valores) {
                tipo_exp = exp.getTipo(e);
                if (null != tipo_exp) {
                    switch (tipo_exp.rol) {
                        case VECTOR:
                            if (tipo_exp.tp == Tipo.tipo.STRING) {
                                vector.addAll((ArrayList) exp.getValorImplicito(e));
                            } else {
                                ArrayList<Object> array = (ArrayList) exp.getValorImplicito(e);
                                array.replaceAll(v -> v.toString());
                                vector.addAll(array);
                            }
                            break;

                        case PRIMITIVO:
                            vector.add(exp.getValorImplicito(e).toString());
                            break;
                        default:
                            Singleton.getInstance().addError("Error en el tipo de dato del vector", linea, columna);
                            return null;
                    }
                }
            }
            return vector;
        } else if (doubleFlag) {
            vector = new NodoVector(new Tipo(Tipo.Rol.VECTOR, Tipo.tipo.NUMERIC));

            for (Expresion exp : valores) {
                tipo_exp = exp.getTipo(e);
                if (null != tipo_exp) {
                    switch (tipo_exp.rol) {
                        case VECTOR:
                            if (tipo_exp.tp == Tipo.tipo.NUMERIC) {
                                vector.addAll((ArrayList) exp.getValorImplicito(e));
                            } else if (tipo_exp.tp == Tipo.tipo.BOOLEAN) {
                                ArrayList<Object> array = (ArrayList) exp.getValorImplicito(e);
                                for (Object val : array) {
                                    vector.add(Boolean.parseBoolean(val.toString().toLowerCase()) ? 1.0 : 0.0);
                                }
                            } else if (tipo_exp.tp == Tipo.tipo.INTEGER) {
                                ArrayList<Object> array = (ArrayList) exp.getValorImplicito(e);
                                array.replaceAll(v -> Double.parseDouble(v.toString()));
                                vector.addAll(array);
                            }
                            break;
                        case PRIMITIVO:
                            if (tipo_exp.tp == Tipo.tipo.NUMERIC) {
                                vector.add(exp.getValorImplicito(e));
                            } else if (tipo_exp.tp == Tipo.tipo.INTEGER) {
                                vector.add(Double.parseDouble(exp.getValorImplicito(e).toString()));
                            } else if (tipo_exp.tp == Tipo.tipo.BOOLEAN) {
                                vector.add(Boolean.parseBoolean(exp.getValorImplicito(e).toString().toLowerCase()) ? 1.0 : 0.0);
                            }
                            break;
                        default:
                            Singleton.getInstance().addError("Error en el tipo de dato del vector", linea, columna);
                            return null;
                    }
                }

            }
            return vector;
        } else if (intFlag) {
            vector = new NodoVector(new Tipo(Tipo.Rol.VECTOR, Tipo.tipo.INTEGER));

            for (Expresion exp : valores) {
                tipo_exp = exp.getTipo(e);
                if (null != tipo_exp) {
                    switch (tipo_exp.rol) {
                        case VECTOR:
                            if (tipo_exp.tp == Tipo.tipo.INTEGER) {
                                vector.addAll((ArrayList) exp.getValorImplicito(e));
                            } else if (tipo_exp.tp == Tipo.tipo.BOOLEAN) {
                                ArrayList<Object> array = (ArrayList) exp.getValorImplicito(e);
                                for (Object val : array) {
                                    vector.add(Boolean.parseBoolean(val.toString().toLowerCase()) ? 1 : 0);
                                }
                            }
                            break;
                        case PRIMITIVO:
                            if (tipo_exp.tp == Tipo.tipo.INTEGER) {
                                vector.add(exp.getValorImplicito(e));
                            } else if (tipo_exp.tp == Tipo.tipo.BOOLEAN) {
                                vector.add(Boolean.parseBoolean(exp.getValorImplicito(e).toString().toLowerCase()) ? 1 : 0);
                            }
                            break;
                        default:
                            Singleton.getInstance().addError("Error en el tipo de dato del vector", linea, columna);
                            return null;
                    }
                }
            }
            return vector;
        } else if (booleanFlag) {
            vector = new NodoVector(new Tipo(Tipo.Rol.VECTOR, Tipo.tipo.BOOLEAN));

            for (Expresion exp : valores) {
                tipo_exp = exp.getTipo(e);
                if (null != tipo_exp) {
                    switch (tipo_exp.rol) {
                        case VECTOR:
                            if (tipo_exp.tp == Tipo.tipo.BOOLEAN) {
                                vector.addAll((ArrayList) exp.getValorImplicito(e));
                            }
                            break;
                        case PRIMITIVO:
                            if (tipo_exp.tp == Tipo.tipo.BOOLEAN) {
                                vector.add(exp.getValorImplicito(e));
                            }
                            break;
                        default:
                            Singleton.getInstance().addError("Error en el tipo de dato del vector", linea, columna);
                            return null;
                    }
                }
            }
            return vector;
        }
        return null;
    }

    @Override
    public Tipo getTipo(Entorno e) {
        Tipo tipo_exp = null;

        for (Expresion exp : valores) {
            tipo_exp = exp.getTipo(e);

            if (null != tipo_exp) {
                switch (tipo_exp.tp) {
                    case LISTA:
                        listaFlag = true;
                        break;
                    case STRING:
                        stringFlag = true;
                        break;
                    case NUMERIC:
                        doubleFlag = true;
                        break;
                    case INTEGER:
                        intFlag = true;
                        break;
                    case BOOLEAN:
                        booleanFlag = true;
                        break;
                    default:
                        break;
                }
            }
        }

        if (listaFlag) {
            this.tipo = new Tipo(Tipo.Rol.LISTA, Tipo.tipo.LISTA);
        } else if (stringFlag) {
            this.tipo = new Tipo(Tipo.Rol.VECTOR, Tipo.tipo.STRING);
        } else if (doubleFlag) {
            this.tipo = new Tipo(Tipo.Rol.VECTOR, Tipo.tipo.NUMERIC);
        } else if (intFlag) {
            this.tipo = new Tipo(Tipo.Rol.VECTOR, Tipo.tipo.INTEGER);
        } else if (booleanFlag) {
            this.tipo = new Tipo(Tipo.Rol.VECTOR, Tipo.tipo.BOOLEAN);
        }

        return tipo;
    }

    @Override
    public int linea() {
        return this.linea;
    }

    @Override
    public int columna() {
        return this.columna;
    }

}
