/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.Vectores;

import ast.expresion.Expresion;

/**
 *
 * @author Kenia
 */
public class Corchetes{
    
    public Expresion expresion;
    public boolean dobleCorchete;

    public Corchetes(Expresion expresion, boolean dobleCorchete) {
        this.expresion = expresion;
        this.dobleCorchete = dobleCorchete;
    }
   
}
