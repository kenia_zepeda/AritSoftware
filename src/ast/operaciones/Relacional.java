/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.operaciones;

import ast.Singleton;
import ast.entorno.Entorno;
import ast.entorno.Tipo;
import ast.expresion.Expresion;
import java.io.Serializable;

/**
 *
 * @author Kenia
 */
public class Relacional extends Operacion implements Serializable {

    private int linea;
    private int columna;
    public Tipo tipo;

    public Relacional(Expresion izq, Expresion der, Operador op, int linea, int columna) {
        super(izq, der, op, linea, columna);
        this.linea = linea;
        this.columna = columna;
    }

    @Override
    public Object getValorImplicito(Entorno ent) {
        Object op1Val = op1.getValorImplicito(ent);
        Object op2Val = op2.getValorImplicito(ent);

        if (op1Val != null && op2Val != null) {
            Tipo.tipo op1Tipo = op1.getTipo(ent).tp;
            Tipo.tipo op2Tipo = op2.getTipo(ent).tp;

            switch (op) {
                case MAYOR_QUE:
                    //Tipo resultante de datos: Decimal
                    if (op1Tipo == Tipo.tipo.INTEGER && op2Tipo == Tipo.tipo.NUMERIC) {
                        return Integer.parseInt(op1Val.toString()) > Double.parseDouble(op2Val.toString());
                    } else if (op1Tipo == Tipo.tipo.NUMERIC && op2Tipo == Tipo.tipo.INTEGER) {
                        return Double.parseDouble(op1Val.toString()) > Integer.parseInt(op2Val.toString());
                  
                    } else if (op1Tipo == Tipo.tipo.BOOLEAN && op2Tipo == Tipo.tipo.NUMERIC) {
                        int o1 = Boolean.parseBoolean(op1Val.toString().toLowerCase()) ? 1 : 0;
                        return o1 > Double.parseDouble(op2Val.toString());
                    } else if (op1Tipo == Tipo.tipo.NUMERIC && op2Tipo == Tipo.tipo.BOOLEAN) {
                        int o2 = Boolean.parseBoolean(op1Val.toString().toLowerCase()) ? 1 : 0;
                        return Double.parseDouble(op1Val.toString()) > o2;
                    } else if (op1Tipo == Tipo.tipo.NUMERIC && op2Tipo == Tipo.tipo.NUMERIC) {
                        return Double.parseDouble(op1Val.toString()) > Double.parseDouble(op2Val.toString());
 
                    } else if (op1Tipo == Tipo.tipo.BOOLEAN && op2Tipo == Tipo.tipo.INTEGER) {
                        int o1 = Boolean.parseBoolean(op1Val.toString().toLowerCase()) ? 1 : 0;
                        return o1 > Integer.parseInt(op2Val.toString());
                    } else if (op1Tipo == Tipo.tipo.INTEGER && op2Tipo == Tipo.tipo.BOOLEAN) {
                        int o2 = Boolean.parseBoolean(op1Val.toString().toLowerCase()) ? 1 : 0;
                        return Integer.parseInt(op1Val.toString()) > o2;
                    } else if (op1Tipo == Tipo.tipo.INTEGER && op2Tipo == Tipo.tipo.INTEGER) {
                        return Integer.parseInt(op1Val.toString()) > Integer.parseInt(op2Val.toString());
                    } //Tipo resultante de datos: Cadena
                    else if (op1Tipo == Tipo.tipo.STRING || op2Tipo == Tipo.tipo.STRING) {
                        return op1Val.toString().length() > op2Val.toString().length();
                    } //Tipo resultante de datos: Bool
                    else if (op1Tipo == Tipo.tipo.BOOLEAN && op2Tipo == Tipo.tipo.BOOLEAN) {
                        Singleton.getInstance().addError("Error de tipos, se utilizo el operador" + " mayor que para dos boolos");
                        return null;
                    }
                    break;
                case MAYORIGUAL_QUE:
                    //Tipo resultante de datos: Decimal
                    if (op1Tipo == Tipo.tipo.INTEGER && op2Tipo == Tipo.tipo.NUMERIC) {
                        return Integer.parseInt(op1Val.toString()) >= Double.parseDouble(op2Val.toString());
                    } else if (op1Tipo == Tipo.tipo.NUMERIC && op2Tipo == Tipo.tipo.INTEGER) {
                        return Double.parseDouble(op1Val.toString()) >= Integer.parseInt(op2Val.toString());
             
                    } else if (op1Tipo == Tipo.tipo.BOOLEAN && op2Tipo == Tipo.tipo.NUMERIC) {
                        int o1 = Boolean.parseBoolean(op1Val.toString().toLowerCase()) ? 1 : 0;
                        return o1 >= Double.parseDouble(op2Val.toString());
                    } else if (op1Tipo == Tipo.tipo.NUMERIC && op2Tipo == Tipo.tipo.BOOLEAN) {
                        int o2 = Boolean.parseBoolean(op1Val.toString().toLowerCase()) ? 1 : 0;
                        return Double.parseDouble(op1Val.toString()) >= o2;
                    } else if (op1Tipo == Tipo.tipo.NUMERIC && op2Tipo == Tipo.tipo.NUMERIC) {
                        return Double.parseDouble(op1Val.toString()) >= Double.parseDouble(op2Val.toString());
   
                    } else if (op1Tipo == Tipo.tipo.BOOLEAN && op2Tipo == Tipo.tipo.INTEGER) {
                        int o1 = Boolean.parseBoolean(op1Val.toString().toLowerCase()) ? 1 : 0;
                        return o1 >= Integer.parseInt(op2Val.toString());
                    } else if (op1Tipo == Tipo.tipo.INTEGER && op2Tipo == Tipo.tipo.BOOLEAN) {
                        int o2 = Boolean.parseBoolean(op1Val.toString().toLowerCase()) ? 1 : 0;
                        return Integer.parseInt(op1Val.toString()) >= o2;
                    } else if (op1Tipo == Tipo.tipo.INTEGER && op2Tipo == Tipo.tipo.INTEGER) {
                        return Integer.parseInt(op1Val.toString()) >= Integer.parseInt(op2Val.toString());
                    } //Tipo resultante de datos: Cadena
                    else if (op1Tipo == Tipo.tipo.STRING || op2Tipo == Tipo.tipo.STRING) {
                        return op1Val.toString().length() >= op2Val.toString().length();
                    } //Tipo resultante de datos: Bool
                    else if (op1Tipo == Tipo.tipo.BOOLEAN && op2Tipo == Tipo.tipo.BOOLEAN) {
                        Singleton.getInstance().addError("Error de tipos, se utilizo el operador" + " mayor que para dos boolos");
                        return null;
                    }
                    break;

                case MENOR_QUE:
                    //Tipo resultante de datos: Decimal
                    if (op1Tipo == Tipo.tipo.INTEGER && op2Tipo == Tipo.tipo.NUMERIC) {
                        return Integer.parseInt(op1Val.toString()) < Double.parseDouble(op2Val.toString());
                    } else if (op1Tipo == Tipo.tipo.NUMERIC && op2Tipo == Tipo.tipo.INTEGER) {
                        return Double.parseDouble(op1Val.toString()) < Integer.parseInt(op2Val.toString());
          
                    } else if (op1Tipo == Tipo.tipo.BOOLEAN && op2Tipo == Tipo.tipo.NUMERIC) {
                        int o1 = Boolean.parseBoolean(op1Val.toString().toLowerCase()) ? 1 : 0;
                        return o1 < Double.parseDouble(op2Val.toString());
                    } else if (op1Tipo == Tipo.tipo.NUMERIC && op2Tipo == Tipo.tipo.BOOLEAN) {
                        int o2 = Boolean.parseBoolean(op1Val.toString().toLowerCase()) ? 1 : 0;
                        return Double.parseDouble(op1Val.toString()) < o2;
                    } else if (op1Tipo == Tipo.tipo.NUMERIC && op2Tipo == Tipo.tipo.NUMERIC) {
                        return Double.parseDouble(op1Val.toString()) < Double.parseDouble(op2Val.toString());
                     //Tipo resultante de datos: Entero
               
                    } else if (op1Tipo == Tipo.tipo.BOOLEAN && op2Tipo == Tipo.tipo.INTEGER) {
                        int o1 = Boolean.parseBoolean(op1Val.toString().toLowerCase()) ? 1 : 0;
                        return o1 < Integer.parseInt(op2Val.toString());
                    } else if (op1Tipo == Tipo.tipo.INTEGER && op2Tipo == Tipo.tipo.BOOLEAN) {
                        int o2 = Boolean.parseBoolean(op1Val.toString().toLowerCase()) ? 1 : 0;
                        return Integer.parseInt(op1Val.toString()) < o2;
                    } else if (op1Tipo == Tipo.tipo.INTEGER && op2Tipo == Tipo.tipo.INTEGER) {

                        int a = Integer.parseInt(op1Val.toString());
                        int b = Integer.parseInt(op2Val.toString());
                        Boolean val = a < b;
                        return val;
                    } //Tipo resultante de datos: Cadena
                    else if (op1Tipo == Tipo.tipo.STRING || op2Tipo == Tipo.tipo.STRING) {
                        return op1Val.toString().length() < op2Val.toString().length();
                    } //Tipo resultante de datos: Bool
                    else if (op1Tipo == Tipo.tipo.BOOLEAN && op2Tipo == Tipo.tipo.BOOLEAN) {
                        Singleton.getInstance().addError("Error de tipos, se utilizo el operador" + " menor que para dos boolos");
                        return null;
                    }
                    break;
                case MENORIGUAL_QUE:
                    //Tipo resultante de datos: Decimal
                    if (op1Tipo == Tipo.tipo.INTEGER && op2Tipo == Tipo.tipo.NUMERIC) {
                        return Integer.parseInt(op1Val.toString()) <= Double.parseDouble(op2Val.toString());
                    } else if (op1Tipo == Tipo.tipo.NUMERIC && op2Tipo == Tipo.tipo.INTEGER) {
                        return Double.parseDouble(op1Val.toString()) <= Integer.parseInt(op2Val.toString());

                    } else if (op1Tipo == Tipo.tipo.BOOLEAN && op2Tipo == Tipo.tipo.NUMERIC) {
                        int o1 = Boolean.parseBoolean(op1Val.toString().toLowerCase()) ? 1 : 0;
                        return o1 <= Double.parseDouble(op2Val.toString());
                    } else if (op1Tipo == Tipo.tipo.NUMERIC && op2Tipo == Tipo.tipo.BOOLEAN) {
                        int o2 = Boolean.parseBoolean(op1Val.toString().toLowerCase()) ? 1 : 0;
                        return Double.parseDouble(op1Val.toString()) <= o2;
                    } else if (op1Tipo == Tipo.tipo.NUMERIC && op2Tipo == Tipo.tipo.NUMERIC) {
                        return Double.parseDouble(op1Val.toString()) <= Double.parseDouble(op2Val.toString());
                     //Tipo resultante de datos: Entero
      
                    } else if (op1Tipo == Tipo.tipo.BOOLEAN && op2Tipo == Tipo.tipo.INTEGER) {
                        int o1 = Boolean.parseBoolean(op1Val.toString().toLowerCase()) ? 1 : 0;
                        return o1 <= Integer.parseInt(op2Val.toString());
                    } else if (op1Tipo == Tipo.tipo.INTEGER && op2Tipo == Tipo.tipo.BOOLEAN) {
                        int o2 = Boolean.parseBoolean(op1Val.toString().toLowerCase()) ? 1 : 0;
                        return Integer.parseInt(op1Val.toString()) <= o2;
                    } else if (op1Tipo == Tipo.tipo.INTEGER && op2Tipo == Tipo.tipo.INTEGER) {
                        return Integer.parseInt(op1Val.toString()) <= Integer.parseInt(op2Val.toString());
                    } //Tipo resultante de datos: Cadena
                    else if (op1Tipo == Tipo.tipo.STRING || op2Tipo == Tipo.tipo.STRING) {
                        return op1Val.toString().length() <= op2Val.toString().length();
                    } //Tipo resultante de datos: Bool
                    else if (op1Tipo == Tipo.tipo.BOOLEAN && op2Tipo == Tipo.tipo.BOOLEAN) {
                        Singleton.getInstance().addError("Error de tipos, se utilizo el operador" + " menor que para dos boolos");
                        return null;
                    }
                    break;
                case IGUAL_IGUAL:
                    //Tipo resultante de datos: Decimal
                    if (op1Tipo == Tipo.tipo.INTEGER && op2Tipo == Tipo.tipo.NUMERIC) {
                        return Integer.parseInt(op1Val.toString()) == Double.parseDouble(op2Val.toString());
                    } else if (op1Tipo == Tipo.tipo.NUMERIC && op2Tipo == Tipo.tipo.INTEGER) {
                        return Double.parseDouble(op1Val.toString()) == Integer.parseInt(op2Val.toString());
       
                    } else if (op1Tipo == Tipo.tipo.BOOLEAN && op2Tipo == Tipo.tipo.NUMERIC) {
                        int o1 = Boolean.parseBoolean(op1Val.toString().toLowerCase()) ? 1 : 0;
                        return o1 == Double.parseDouble(op2Val.toString());
                    } else if (op1Tipo == Tipo.tipo.NUMERIC && op2Tipo == Tipo.tipo.BOOLEAN) {
                        int o2 = Boolean.parseBoolean(op1Val.toString().toLowerCase()) ? 1 : 0;
                        return Double.parseDouble(op1Val.toString()) == o2;
                    } else if (op1Tipo == Tipo.tipo.NUMERIC && op2Tipo == Tipo.tipo.NUMERIC) {
                        return Double.parseDouble(op1Val.toString()) == Double.parseDouble(op2Val.toString());
                     //Tipo resultante de datos: Entero

                    } else if (op1Tipo == Tipo.tipo.BOOLEAN && op2Tipo == Tipo.tipo.INTEGER) {
                        int o1 = Boolean.parseBoolean(op1Val.toString().toLowerCase()) ? 1 : 0;
                        return o1 == Integer.parseInt(op2Val.toString());
                    } else if (op1Tipo == Tipo.tipo.INTEGER && op2Tipo == Tipo.tipo.BOOLEAN) {
                        int o2 = Boolean.parseBoolean(op1Val.toString().toLowerCase()) ? 1 : 0;
                        return Integer.parseInt(op1Val.toString()) == o2;
                    } else if (op1Tipo == Tipo.tipo.INTEGER && op2Tipo == Tipo.tipo.INTEGER) {
                        return Integer.parseInt(op1Val.toString()) == Integer.parseInt(op2Val.toString());
                    } //Tipo resultante de datos: Cadena
                    else if (op1Tipo == Tipo.tipo.STRING || op2Tipo == Tipo.tipo.STRING) {
                        return op1Val.toString().equals(op2Val.toString());
                    } //Tipo resultante de datos: Bool
                    else if (op1Tipo == Tipo.tipo.BOOLEAN && op2Tipo == Tipo.tipo.BOOLEAN) {
                        return Boolean.parseBoolean(op1Val.toString().toLowerCase()) == Boolean.parseBoolean(op2Val.toString().toLowerCase());
                    }
                    break;
                case DIFERENTE_QUE:
                    //Tipo resultante de datos: Decimal
                    if (op1Tipo == Tipo.tipo.INTEGER && op2Tipo == Tipo.tipo.NUMERIC) {
                        return Integer.parseInt(op1Val.toString()) != Double.parseDouble(op2Val.toString());
                    } else if (op1Tipo == Tipo.tipo.NUMERIC && op2Tipo == Tipo.tipo.INTEGER) {

                    } else if (op1Tipo == Tipo.tipo.BOOLEAN && op2Tipo == Tipo.tipo.NUMERIC) {
                        int o1 = Boolean.parseBoolean(op1Val.toString().toLowerCase()) ? 1 : 0;
                        return o1 == Double.parseDouble(op2Val.toString());
                    } else if (op1Tipo == Tipo.tipo.NUMERIC && op2Tipo == Tipo.tipo.BOOLEAN) {
                        int o2 = Boolean.parseBoolean(op1Val.toString().toLowerCase()) ? 1 : 0;
                        return Double.parseDouble(op1Val.toString()) != o2;
                    } else if (op1Tipo == Tipo.tipo.NUMERIC && op2Tipo == Tipo.tipo.NUMERIC) {
                        return Double.parseDouble(op1Val.toString()) != Double.parseDouble(op2Val.toString());
              
                    } else if (op1Tipo == Tipo.tipo.BOOLEAN && op2Tipo == Tipo.tipo.INTEGER) {
                        int o1 = Boolean.parseBoolean(op1Val.toString().toLowerCase()) ? 1 : 0;
                        return o1 != Integer.parseInt(op2Val.toString());
                    } else if (op1Tipo == Tipo.tipo.INTEGER && op2Tipo == Tipo.tipo.BOOLEAN) {
                        int o2 = Boolean.parseBoolean(op1Val.toString().toLowerCase()) ? 1 : 0;
                        return Integer.parseInt(op1Val.toString()) != o2;
                    } else if (op1Tipo == Tipo.tipo.INTEGER && op2Tipo == Tipo.tipo.INTEGER) {
                        return Integer.parseInt(op1Val.toString()) != Integer.parseInt(op2Val.toString());
                    } //Tipo resultante de datos: Cadena
                    else if (op1Tipo == Tipo.tipo.STRING || op2Tipo == Tipo.tipo.STRING) {
                        return !op1Val.toString().equals(op2Val.toString());
                    } //Tipo resultante de datos: Bool
                    else if (op1Tipo == Tipo.tipo.BOOLEAN && op2Tipo == Tipo.tipo.BOOLEAN) {
                        return Boolean.parseBoolean(op1Val.toString().toLowerCase()) != Boolean.parseBoolean(op2Val.toString().toLowerCase());
                    }
                    break;
                default:
                    return false;

            }

        }
        return false;
    }

    @Override
    public Tipo getTipo(Entorno e) {
        return new Tipo(Tipo.Rol.PRIMITIVO, Tipo.tipo.BOOLEAN);
    }

    @Override
    public int columna() {
        return this.columna;
    }

    @Override
    public int linea() {
        return this.linea;
    }
}
