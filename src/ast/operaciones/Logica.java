/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.operaciones;

import ast.Singleton;
import ast.entorno.Entorno;
import ast.entorno.Tipo;
import ast.expresion.Expresion;
import java.io.Serializable;

/**
 *
 * @author Kenia
 */
public class Logica extends Operacion implements Serializable {

    private int linea;
    private int columna;

    public Logica(Expresion izq, Expresion der, Operador op, int linea, int columna) {
        super(izq, der, op, linea, columna);
        this.linea = linea;
        this.columna = columna;
    }

    public Logica(Expresion izq, Operador op, int linea, int columna) {
        super(izq, op);
        this.linea = linea;
        this.columna = columna;
    }

    @Override
    public Object getValorImplicito(Entorno ent) {
        switch (op) {
            case AND:
                return and(ent);
            case OR:
                return or(ent);
            case NOT:
                return not(ent);
            default:
                return null;
        }

    }

    public Object and(Entorno ent) {

        Object op1Val = op1.getValorImplicito(ent);
        Object op2Val = op2.getValorImplicito(ent);

        if (op1Val != null && op2Val != null) {
            Tipo.tipo op1Tipo = op1.getTipo(ent).tp;
            Tipo.tipo op2Tipo = op2.getTipo(ent).tp;

            if (op1Tipo == Tipo.tipo.BOOLEAN && op2Tipo == Tipo.tipo.BOOLEAN) {
                boolean v1 = Boolean.valueOf(op1Val.toString().toLowerCase());
                boolean v2 = Boolean.valueOf(op2Val.toString().toLowerCase());
                return Boolean.logicalAnd(v1, v2);
            } else {
                Singleton.getInstance().addError("Tipo de dato incorrecto, no se puede realizar AND", linea, columna);
            }

        }
        return false;
    }

    public Object or(Entorno ent) {

        Object op1Val = op1.getValorImplicito(ent);
        Object op2Val = op2.getValorImplicito(ent);

        if (op1Val != null && op2Val != null) {
            Tipo.tipo op1Tipo = op1.getTipo(ent).tp;
            Tipo.tipo op2Tipo = op2.getTipo(ent).tp;

            if (op1Tipo == Tipo.tipo.BOOLEAN && op2Tipo == Tipo.tipo.BOOLEAN) {
                boolean v1 = Boolean.parseBoolean(op1Val.toString().toLowerCase());
                boolean v2 = Boolean.parseBoolean(op2Val.toString().toLowerCase());
                return Boolean.logicalOr(v1, v2);
            } else {
                Singleton.getInstance().addError("Tipo de dato incorrecto, no se puede realizar OR", linea, columna);
            }

        }
        return false;
    }

    public Object not(Entorno ent) {

        Object op1Val = op1.getValorImplicito(ent);

        if (op1Val != null) {
            Tipo.tipo op1Tipo = op1.getTipo(ent).tp;

            if (op1Tipo == Tipo.tipo.BOOLEAN) {
                boolean v1 = Boolean.parseBoolean(op1Val.toString().toLowerCase());
                return !v1;
            } else {
                Singleton.getInstance().addError("Tipo de dato incorrecto, no se puede realizar NOT", linea, columna);
            }

        }
        return false;
    }

    @Override
    public int columna() {
        return this.columna;
    }

    @Override
    public int linea() {
        return this.linea;
    }

    @Override
    public Tipo getTipo(Entorno e) {
        return new Tipo(Tipo.Rol.PRIMITIVO, Tipo.tipo.BOOLEAN);
    }

}
