/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.operaciones;

import ast.expresion.Expresion;
import java.io.Serializable;

/**
 *
 * @author Kenia
 */
public abstract class Operacion implements Expresion, Serializable{

    public Expresion op1;
    public Expresion op2;

    public Operador op;

    public enum Operador {
        SUMA,
        RESTA,
        MULTIPLICACION,
        DIVISION,
        POTENCIA,
        MODULO,
        MENOS_UNARIO,
        MAYOR_QUE,
        MENOR_QUE,
        MAYORIGUAL_QUE,
        MENORIGUAL_QUE,
        IGUAL_IGUAL,
        DIFERENTE_QUE,
        OR,
        AND,
        NOT,
        MENOSUNARIO,
        DESCONOCIDO;

    }

    public Operacion(Expresion izq, Expresion der, Operador op, int line, int colm) {
        this.op1 = izq;
        this.op2 = der;
        this.op = op;

    }

    public Operacion(Expresion izq, Operador op) {
        this.op1 = izq;
        this.op2 = null;
        this.op = op;
    }

    public Operacion() {
        this.op1 = null;
        this.op2 = null;
        this.op = null;

    }

}
