/*
 * To change this license heaop2, choose License Heaop2s in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.operaciones;

import ast.Singleton;
import ast.entorno.Entorno;
import ast.entorno.Tipo;
import ast.expresion.Expresion;
import java.io.Serializable;

/**
 *
 * @author Kenia
 */
public class Aritmetica extends Operacion implements Serializable {

    public int linea = 0;
    public int columna = 0;
    public Tipo tipo = null;

    public Aritmetica(Expresion op1, Expresion op2, Operador op, int linea, int columna) {
        super(op1, op2, op, linea, columna);
        this.linea = linea;
        this.columna = columna;
    }

    public Aritmetica() {
    }

    @Override
    public Object getValorImplicito(Entorno ent) {

        if (op1 != null && op2 != null) {

            switch (op) {
                case SUMA:
                    return suma(ent);
                case RESTA:
                    return resta(ent);
                case MULTIPLICACION:
                    return mult(ent);
                case DIVISION:
                    return div(ent);
                case POTENCIA:
                    return potencia(ent);
                case MODULO:
                    return modulo(ent);
                default:
                    return null;
            }
        } else {
            //  Singleton.getInstance().addError("El simbolo \"" + nombre + "\" no ha sido declarado en el entorno actual ni en alguno externo", e.idClase, linea, columna);

        }

        return null;
    }

    public Object suma(Entorno ent) {

        Object op1Val = op1.getValorImplicito(ent);
        Object op2Val = op2.getValorImplicito(ent);

        if (op1Val != null && op2Val != null) {
            Tipo.tipo op1Tipo = op1.getTipo(ent).tp;
            Tipo.tipo op2Tipo = op2.getTipo(ent).tp;

            if (op1Tipo == Tipo.tipo.STRING || op2Tipo == Tipo.tipo.STRING) {
                tipo = new Tipo(Tipo.Rol.PRIMITIVO, Tipo.tipo.STRING);
                return op1Val.toString() + op2Val.toString();
            } else if (op1Tipo == Tipo.tipo.BOOLEAN || op2Tipo == Tipo.tipo.BOOLEAN) {
                Singleton.getInstance().addError("Tipo booleano no se puede sumar", linea, columna);
            } else if (op1Tipo == Tipo.tipo.NUMERIC || op2Tipo == Tipo.tipo.NUMERIC) {

                double v1 = Double.parseDouble(op1Val.toString());
                double v2 = Double.parseDouble(op2Val.toString());
                tipo = new Tipo(Tipo.Rol.PRIMITIVO, Tipo.tipo.NUMERIC);
                return v1 + v2;

            } else if (op1Tipo == Tipo.tipo.INTEGER || op2Tipo == Tipo.tipo.INTEGER) {

                int v1 = Integer.parseInt(op1Val.toString());
                int v2 = Integer.parseInt(op2Val.toString());
                tipo = new Tipo(Tipo.Rol.PRIMITIVO, Tipo.tipo.INTEGER);
                return v1 + v2;

            } else {
                Singleton.getInstance().addError("Tipo de dato incorrecto, no se puede realizar suma", linea, columna);
                return null;
            }

        }
        return null;
    }

    public Object resta(Entorno ent) {
        Object op1Val = op1.getValorImplicito(ent);
        Object op2Val = op2.getValorImplicito(ent);

        if (op1Val != null && op2Val != null) {
            Tipo.tipo op1Tipo = op1.getTipo(ent).tp;
            Tipo.tipo op2Tipo = op2.getTipo(ent).tp;

            if (op1Tipo == Tipo.tipo.STRING || op2Tipo == Tipo.tipo.STRING) {
                Singleton.getInstance().addError("Tipo de dato String no se puede restar", linea, columna);
            } else if (op1Tipo == Tipo.tipo.BOOLEAN || op2Tipo == Tipo.tipo.BOOLEAN) {
                Singleton.getInstance().addError("Tipo de dato booleano no se puede restar", linea, columna);
            } else if (op1Tipo == Tipo.tipo.NUMERIC || op2Tipo == Tipo.tipo.NUMERIC) {

                double v1 = Double.parseDouble(op1Val.toString());
                double v2 = Double.parseDouble(op2Val.toString());
                tipo = new Tipo(Tipo.Rol.PRIMITIVO, Tipo.tipo.NUMERIC);
                return v1 - v2;

            } else if (op1Tipo == Tipo.tipo.INTEGER || op2Tipo == Tipo.tipo.INTEGER) {

                int v1 = Integer.parseInt(op1Val.toString());
                int v2 = Integer.parseInt(op2Val.toString());
                tipo = new Tipo(Tipo.Rol.PRIMITIVO, Tipo.tipo.INTEGER);
                return v1 - v2;

            } else {
                Singleton.getInstance().addError("Tipo de dato incorrecto, no se puede realizar resta", linea, columna);
                return null;
            }
        }
        return null;
    }

    public Object mult(Entorno ent) {

        Object op1Val = op1.getValorImplicito(ent);
        Object op2Val = op2.getValorImplicito(ent);

        if (op1Val != null && op2Val != null) {
            Tipo.tipo op1Tipo = op1.getTipo(ent).tp;
            Tipo.tipo op2Tipo = op2.getTipo(ent).tp;

            if (op1Tipo == Tipo.tipo.STRING || op2Tipo == Tipo.tipo.STRING) {
                Singleton.getInstance().addError("Tipo de dato String no se puede multiplicar", linea, columna);
            } else if (op1Tipo == Tipo.tipo.BOOLEAN || op2Tipo == Tipo.tipo.BOOLEAN) {
                Singleton.getInstance().addError("Tipo de dato booleano no se puede multiplicar", linea, columna);
            } else if (op1Tipo == Tipo.tipo.NUMERIC || op2Tipo == Tipo.tipo.NUMERIC) {
                double v1 = Double.parseDouble(op1Val.toString());
                double v2 = Double.parseDouble(op2Val.toString());
                tipo = new Tipo(Tipo.Rol.PRIMITIVO, Tipo.tipo.NUMERIC);
                return v1 * v2;
            } else if (op1Tipo == Tipo.tipo.INTEGER || op2Tipo == Tipo.tipo.INTEGER) {
                int v1 = Integer.parseInt(op1Val.toString());
                int v2 = Integer.parseInt(op2Val.toString());
                tipo = new Tipo(Tipo.Rol.PRIMITIVO, Tipo.tipo.INTEGER);
                return v1 * v2;
            } else {
                Singleton.getInstance().addError("Tipo de dato incorrecto, no se puede realizar multiplicación", linea, columna);
                return null;
            }

        }
        return null;
    }

    public Object div(Entorno ent) {

        Object op1Val = op1.getValorImplicito(ent);
        Object op2Val = op2.getValorImplicito(ent);

        if (op1Val != null && op2Val != null) {
            Tipo.tipo op1Tipo = op1.getTipo(ent).tp;
            Tipo.tipo op2Tipo = op2.getTipo(ent).tp;

            if (op1Tipo == Tipo.tipo.STRING || op2Tipo == Tipo.tipo.STRING) {
                Singleton.getInstance().addError("Tipo de dato String no se puede dividir", linea, columna);
            } else if (op1Tipo == Tipo.tipo.BOOLEAN || op2Tipo == Tipo.tipo.BOOLEAN) {
                Singleton.getInstance().addError("Tipo de dato booleano no se puede dividir", linea, columna);
            } else if (op1Tipo == Tipo.tipo.NUMERIC || op2Tipo == Tipo.tipo.NUMERIC) {

                double v1 = Double.parseDouble(op1Val.toString());
                double v2 = Double.parseDouble(op2Val.toString());
                tipo = new Tipo(Tipo.Rol.PRIMITIVO, Tipo.tipo.NUMERIC);
                return v1 / v2;

            } else if (op1Tipo == Tipo.tipo.INTEGER || op2Tipo == Tipo.tipo.INTEGER) {

                int v1 = Integer.parseInt(op1Val.toString());
                int v2 = Integer.parseInt(op2Val.toString());
                tipo = new Tipo(Tipo.Rol.PRIMITIVO, Tipo.tipo.INTEGER);
                return v1 / v2;
            } else {
                Singleton.getInstance().addError("Tipo de dato incorrecto, no se puede realizar división", linea, columna);
                return null;
            }

        }
        return null;
    }

    public Object potencia(Entorno ent) {
        Object op1Val = op1.getValorImplicito(ent);
        Object op2Val = op2.getValorImplicito(ent);

        if (op1Val != null && op2Val != null) {
            Tipo.tipo op1Tipo = op1.getTipo(ent).tp;
            Tipo.tipo op2Tipo = op2.getTipo(ent).tp;

            if (op1Tipo == Tipo.tipo.STRING || op2Tipo == Tipo.tipo.STRING) {
                Singleton.getInstance().addError("Tipo de dato String no se puede operar potencia", linea, columna);
            } else if (op1Tipo == Tipo.tipo.BOOLEAN || op2Tipo == Tipo.tipo.BOOLEAN) {
                Singleton.getInstance().addError("Tipo de dato booleano no se puede operar potencia", linea, columna);
            } else if (op1Tipo == Tipo.tipo.NUMERIC || op2Tipo == Tipo.tipo.NUMERIC) {
                double v1 = Double.parseDouble(op1Val.toString());
                double v2 = Double.parseDouble(op2Val.toString());
                tipo = new Tipo(Tipo.Rol.PRIMITIVO, Tipo.tipo.NUMERIC);
                return Math.pow(v1, v2);

            } else if (op1Tipo == Tipo.tipo.INTEGER || op2Tipo == Tipo.tipo.NUMERIC) {

                int v1 = Integer.parseInt(op1Val.toString());
                int v2 = Integer.parseInt(op2Val.toString());
                tipo = new Tipo(Tipo.Rol.PRIMITIVO, Tipo.tipo.NUMERIC);
                return Math.pow(v1, v2);

            } else {
                Singleton.getInstance().addError("Tipo de dato incorrecto, no se puede realizar potencia", linea, columna);
                return null;
            }
        }
        return null;
    }

    public Object modulo(Entorno ent) {
        Object op1Val = op1.getValorImplicito(ent);
        Object op2Val = op2.getValorImplicito(ent);

        if (op1Val != null && op2Val != null) {
            Tipo.tipo op1Tipo = op1.getTipo(ent).tp;
            Tipo.tipo op2Tipo = op2.getTipo(ent).tp;

            if (op1Tipo == Tipo.tipo.STRING || op2Tipo == Tipo.tipo.STRING) {
                Singleton.getInstance().addError("Tipo de dato String no se puede operar módulo", linea, columna);
            } else if (op1Tipo == Tipo.tipo.BOOLEAN || op2Tipo == Tipo.tipo.BOOLEAN) {
                Singleton.getInstance().addError("Tipo de dato booleano no se puede operar módulo", linea, columna);
            } else if (op1Tipo == Tipo.tipo.NUMERIC || op2Tipo == Tipo.tipo.NUMERIC) {

                double v1 = Double.parseDouble(op1Val.toString());
                double v2 = Double.parseDouble(op2Val.toString());
                tipo = new Tipo(Tipo.Rol.PRIMITIVO, Tipo.tipo.NUMERIC);
                return v1 % v2;

            } else if (op1Tipo == Tipo.tipo.INTEGER || op2Tipo == Tipo.tipo.INTEGER) {

                int v1 = Integer.parseInt(op1Val.toString());
                int v2 = Integer.parseInt(op2Val.toString());
                tipo = new Tipo(Tipo.Rol.PRIMITIVO, Tipo.tipo.INTEGER);
                return v1 % v2;

            } else {
                Singleton.getInstance().addError("Tipo de dato incorrecto, no se puede realizar módulo", linea, columna);
                return null;
            }
        }

        return null;
    }

    @Override
    public Tipo getTipo(Entorno ent) {

        tipo = new Tipo(Tipo.Rol.DESCONOCIDO, Tipo.tipo.DESCONOCIDO);

        switch (op) {
            case SUMA:
                suma(ent);
                break;
            case RESTA:
                resta(ent);
                break;
            case MULTIPLICACION:
                mult(ent);
                break;
            case DIVISION:
                div(ent);
                break;
            case POTENCIA:
                potencia(ent);
                break;
            case MODULO:
                modulo(ent);
                break;

        }

        return tipo;
    }

    @Override
    public int linea() {
        return this.linea;
    }

    @Override
    public int columna() {
        return this.columna;
    }

}
