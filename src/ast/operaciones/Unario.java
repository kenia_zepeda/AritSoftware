/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.operaciones;

import ast.Singleton;
import ast.entorno.Entorno;
import ast.entorno.Tipo;
import ast.expresion.Expresion;
import java.io.Serializable;

/**
 *
 * @author Kenia
 */
public class Unario extends Operacion implements Serializable {

    int linea, columna;
    public Tipo tipo;


    public Unario(Expresion izq, Operador op, int linea, int columna) {
        super(izq, op);
        this.linea = linea;
        this.columna = columna;
    }

    @Override
    public Object getValorImplicito(Entorno ent) {

        Object opU = op1.getValorImplicito(ent);

        if (opU instanceof Double) {
            tipo = new Tipo(Tipo.Rol.PRIMITIVO,Tipo.tipo.NUMERIC);
            return 0.0 - (double) opU;
        } else if (opU instanceof Integer) {
            tipo = new Tipo(Tipo.Rol.PRIMITIVO,Tipo.tipo.INTEGER);
            return 0.0 - (int) opU;
        } else if (opU instanceof Character) {
            tipo = new Tipo(Tipo.Rol.PRIMITIVO,Tipo.tipo.INTEGER);
            return 0 - (int) ((char) opU);
        } else {
            Singleton.getInstance().addError("Error de tipos, se utilizo el operador" + " menos unario incorrectamente", linea, columna);
            return null;
        }
    }

    @Override
    public Tipo getTipo(Entorno e) {
        getValorImplicito(e);
        return tipo;
    }

    @Override
    public int linea() {
        return this.linea;
    }

    @Override
    public int columna() {
        return this.columna;
    }

}
