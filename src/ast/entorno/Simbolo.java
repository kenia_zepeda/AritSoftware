/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.entorno;

import ast.Vectores.NodoVector;
import java.io.Serializable;

/**
 *
 * @author Kenia
 */
public class Simbolo implements Serializable {

    public Tipo tipo;
    public String id;
    public Object valor;
    public Tipo.Rol rol;
    public int dim;

    /**
     * Contructor para declaración y asiganción de variables
     *
     * @param tipo
     * @param id
     * @param valor
     */
    public Simbolo(Tipo tipo, String id, Object valor) {
        this.id = id;
        this.tipo = tipo;
        this.valor = valor;
        this.rol = tipo.rol;
    }

    /**
     * Constructor para declaración de variables
     *
     * @param tipo
     * @param id
     */
    public Simbolo(Tipo tipo, String id) {
        this.tipo = tipo;
        this.id = id;
        this.rol = tipo.rol;
    }

    /**
     * Constructor para parametros de funciones
     *
     * @param id
     * @param valor
     * @param rol
     */
    public Simbolo(String id, Object valor, Tipo.Rol rol) {
        this.id = id;
        this.valor = valor;
        this.rol = rol;
        this.tipo = new Tipo(rol, Tipo.tipo.DESCONOCIDO);
    }

    /**
     * Constructor para funciones
     *
     * @param id
     * @param rol
     */
    public Simbolo(String id) {
        this.id = id;
        this.rol = Tipo.Rol.FUNCION;
        this.tipo = new Tipo(Tipo.Rol.FUNCION, Tipo.tipo.DESCONOCIDO);
    }

    public Tipo getTipo() {
        return tipo;
    }

    public void setTipo(Tipo tipo) {
        this.tipo = tipo;
        this.rol = tipo.rol;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Object getValor() {
        return valor;
    }

    public NodoVector getVector() {
        NodoVector vector_sim = new NodoVector(tipo);

        if (tipo.rol == Tipo.Rol.PRIMITIVO) {
            vector_sim.add(valor);

        } else if (tipo.rol == Tipo.Rol.VECTOR) {
            vector_sim = (NodoVector) valor;

        }

        return vector_sim;
    }

    public void setValor(Object valor) {
        this.valor = valor;
    }

}
