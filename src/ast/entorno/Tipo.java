/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.entorno;

import java.io.Serializable;

/**
 *
 * @author Kenia
 */
public class Tipo implements Serializable {

    public Rol rol;
    public Tipo.tipo tp;

    public static enum tipo {
        INTEGER,
        BOOLEAN,
        NUMERIC,
        STRING,
        LISTA,
        CONCATENAR,
        MATRIZ,
        ARREGLO,
        FUNCION,
        DESCONOCIDO,
        DEFAULT,
        NULL
    }

    public static enum Rol {
        PRIMITIVO,
        VECTOR,
        LISTA,
        MATRIZ,
        ARREGLO,
        FUNCION,
        METODO,
        PARAMETRO,
        LLAMADAFUNCION,
        DEFAULT,
        NULL,
        DESCONOCIDO
    }

    public Tipo(tipo tp) {
        this.rol = Rol.PRIMITIVO;
        this.tp = tp;
    }

    public Tipo(Rol rol, tipo tp) {
        this.rol = rol;
        this.tp = tp;
    }

}
