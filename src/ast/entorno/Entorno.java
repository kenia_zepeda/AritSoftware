/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.entorno;

import ast.Singleton;
import java.io.Serializable;
import java.util.HashMap;

/**
 *
 * @author Kenia
 */
public class Entorno implements Serializable{

    public Entorno padre;
    public HashMap<String, Simbolo> tabla;

    public Entorno(Entorno ent) {
        this.padre = ent;
        tabla = new HashMap<>();
    }

    public Entorno() {
        this.padre = null;
        tabla = new HashMap<>();
    }

    public Simbolo getSimbolo(String id, int linea, int columna) {
        for (Entorno e = this; e != null; e = e.getPadre()) {
            Simbolo encontrado = (e.getTabla().get(id));
            if (encontrado != null) {
                return encontrado;
            }
        }
        Singleton.getInstance().addError("El simbolo \"" + id + "\" no ha sido declarado en el entorno actual ni en alguno externo", linea, columna);
        return null;
    }

    /*public void reemplazar(String id, Object valor, int linea, int columna) {
        for (Entorno e = this; e != null; e = e.getPadre()) {
            Simbolo encontrado = (Simbolo) (e.getTabla().get(id));
            if (encontrado != null) {
                getTabla().get(id).valor = valor;                
            }
        }
        Singleton.getInstance().addError("La variable " + id + " no existe en este entorno, por lo "
                + "que no puede asignársele un valor.", ent.idClase, linea, columna);
    }*/
    public final boolean existeEnActual(String id) {
        Simbolo encontrado = (getTabla().get(id));
        return encontrado != null;
    }

    public final boolean existe(String id) {
        for (Entorno e = this; e != null; e = e.getPadre()) {
            if (e.getTabla().containsKey(id)) {
                return true;
            }
        }
        return false;
    }

    public final void agregar(String id, Simbolo simbolo) {
        getTabla().put(id, simbolo);
    }

    public Entorno getPadre() {
        return padre;
    }

    public void setPadre(Entorno padre) {
        this.padre = padre;
    }

    public HashMap<String, Simbolo> getTabla() {
        return tabla;
    }

    public void setTabla(HashMap<String, Simbolo> tabla) {
        this.tabla = tabla;
    }

    void imprimir() {

    }

}
