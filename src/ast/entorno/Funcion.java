/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.entorno;

import ast.NodoAst;
import ast.expresion.Expresion;
import ast.expresion.Identificador;
import ast.instrucciones.Instruccion;
import java.util.LinkedList;

/**
 *
 * @author Kenia
 */
public class Funcion extends Simbolo implements Instruccion {

    public LinkedList<Simbolo> listaParametros;
    public LinkedList<NodoAst> instrucciones;
    public int linea, columna;

    public Funcion(String id, LinkedList<Simbolo> listaParametros, LinkedList<NodoAst> instrucciones, int linea, int columna) {
        super(id);

        this.listaParametros = listaParametros;
        this.instrucciones = instrucciones;
        this.linea = linea;
        this.columna = columna;
        this.id = id;
    }

    public Funcion(String id, String b, LinkedList<NodoAst> instrucciones, int linea, int columna) {
        super(id);

        this.listaParametros = new LinkedList<Simbolo>();
        listaParametros.add(new Simbolo(b, null, Tipo.Rol.PARAMETRO));
        this.instrucciones = instrucciones;
        this.linea = linea;
        this.columna = columna;
        this.id = id;
    }

    public Funcion(String id, Expresion b, LinkedList<NodoAst> instrucciones, int linea, int columna) {
        super(id);

        this.listaParametros = new LinkedList<Simbolo>();
        Identificador ident = (Identificador) b;

        listaParametros.add(new Simbolo(ident.nombre, null, Tipo.Rol.PARAMETRO));
        this.instrucciones = instrucciones;
        this.linea = linea;
        this.columna = columna;
        this.id = id;
    }

    public Funcion(String id, LinkedList<NodoAst> instrucciones, int linea, int columna) {
        super(id);

        this.listaParametros = new LinkedList<Simbolo>();
        this.instrucciones = instrucciones;
        this.linea = linea;
        this.columna = columna;
        this.id = id;
    }

    @Override
    public Object ejecutar(Entorno ent) {
     
        for (NodoAst nodo : instrucciones) {
            if (nodo instanceof Instruccion) {
                Instruccion ins = (Instruccion) nodo;
                Object result = ins.ejecutar(ent);
                if (result != null) {
                    return result;
                }
            } else if (nodo instanceof Expresion) {
                Expresion expr = (Expresion) nodo;
                Object result = expr.getValorImplicito(ent);
                if (result != null) {
                    return result;
                }
            }
        }
        return null;
    }

    @Override
    public int linea() {
        return this.linea;
    }

    @Override
    public int columna() {
        return this.columna;
    }

}
