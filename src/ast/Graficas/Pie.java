/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.Graficas;

import ast.Singleton;
import ast.Vectores.NodoVector;
import ast.entorno.Entorno;
import ast.instrucciones.Instruccion;
import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JPanel;
import org.knowm.xchart.PieChart;
import org.knowm.xchart.PieChartBuilder;
import org.knowm.xchart.SwingWrapper;
import org.knowm.xchart.XChartPanel;

/**
 *
 * @author Kenia
 */
public class Pie implements Instruccion {

    public NodoVector valores;
    public NodoVector etiquetas;
    public String titulo;
    public int linea, columna;

    public Pie(NodoVector valores, NodoVector etiquetas, String titulo, int linea, int columna) {
        this.valores = valores;
        this.etiquetas = etiquetas;
        this.titulo = titulo;
        this.linea = linea;
        this.columna = columna;
    }

    @Override
    public Object ejecutar(Entorno e) {
        PieChart chart = new PieChartBuilder().width(800).height(600).title(getClass().getSimpleName()).build();

        if (valores.size() == etiquetas.size()) {
            for (int i = 0; i < valores.size(); i++) {
                chart.addSeries(etiquetas.get(i).toString(), Double.parseDouble(valores.get(i).toString()));
            }
        } else {
            Singleton.getInstance().addError("La catidad de valores y etiquetas no es igual", linea, columna);
            return null;

        }

        JPanel panel = new XChartPanel<>(chart);
        JFrame frame = new JFrame(titulo);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.add(panel);
        frame.pack();
        frame.setVisible(true);

        return null;
    }

    @Override
    public int linea() {
        return linea;
    }

    @Override
    public int columna() {
        return columna;
    }

}
