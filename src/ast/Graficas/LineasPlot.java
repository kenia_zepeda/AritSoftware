/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.Graficas;

import ast.Vectores.NodoVector;
import ast.entorno.Entorno;
import ast.instrucciones.Instruccion;
import ast.matrix.NodoMatrix;
import java.util.Arrays;
import javax.swing.JFrame;
import javax.swing.JPanel;
import org.knowm.xchart.XChartPanel;
import org.knowm.xchart.XYChart;
import org.knowm.xchart.XYChartBuilder;
import org.knowm.xchart.XYSeries.XYSeriesRenderStyle;

/**
 *
 * @author Kenia
 */
public class LineasPlot implements Instruccion {

    public NodoVector valores;
    public String tipo;
    public String tituloX;
    public String tituloY;
    public String titulo;
    public NodoVector min_max;
    public int linea, columna;

    public LineasPlot(NodoVector valores, String titulo, String tituloX, String tituloY, NodoVector min_max, int linea, int columna) {
        this.valores = valores;
        this.tituloX = tituloX;
        this.tituloY = tituloY;
        this.titulo = titulo;
        this.min_max = min_max;
        this.tipo = null;
    }

    public LineasPlot(NodoVector etiquetas, String tipo, String tituloX, String tituloY, String titulo, int linea, int columna) {
        this.valores = etiquetas;
        this.tipo = tipo;
        this.tituloX = tituloX;
        this.tituloY = tituloY;
        this.titulo = titulo;
        this.linea = linea;
        this.columna = columna;
        this.min_max = null;
    }

    @Override
    public Object ejecutar(Entorno e) {
        if (min_max == null) {

            XYChart chart = new XYChartBuilder().width(800).height(600).title(titulo).xAxisTitle(tituloX).yAxisTitle(tituloY).build();

            Double[] v1 = new Double[valores.size()];
            Integer[] v2 = new Integer[valores.size()];

            for (int i = 0; i < valores.size(); i++) {
                v1[i] = Double.parseDouble(valores.get(i).toString());
                v2[i] = i + 1;
            }

            chart.addSeries(titulo, Arrays.asList(v2), Arrays.asList(v1));
            JPanel panel = new XChartPanel<>(chart);
            JFrame frame = new JFrame(titulo);
            frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            frame.add(panel);
            frame.pack();
            frame.setVisible(true);
        } else {

            XYChart chart = new XYChartBuilder().width(800).height(600).title(titulo).xAxisTitle(tituloX).yAxisTitle(tituloY).build();
            Double[] v1 = new Double[valores.size()];
            Integer[] v2 = new Integer[valores.size()];
            // Customize Chart
            chart.getStyler().setDefaultSeriesRenderStyle(XYSeriesRenderStyle.Scatter);
            chart.getStyler().setChartTitleVisible(false);
            chart.getStyler().setMarkerSize(15);

            for (int i = 0; i < valores.size(); i++) {
                v1[i] = Double.parseDouble(valores.get(i).toString());
                v2[i] = i + 1;
            }

            chart.addSeries(titulo, Arrays.asList(v2), Arrays.asList(v1));
            JPanel panel = new XChartPanel<>(chart);
            JFrame frame = new JFrame(titulo);
            frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            frame.add(panel);
            frame.pack();
            frame.setVisible(true);
        }

        return null;
    }

    @Override
    public int linea() {
        return this.linea;
    }

    @Override
    public int columna() {
        return this.columna;
    }

}
