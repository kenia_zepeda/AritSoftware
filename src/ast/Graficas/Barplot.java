/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.Graficas;

import ast.Singleton;
import ast.Vectores.NodoVector;
import ast.entorno.Entorno;
import ast.instrucciones.Instruccion;
import java.util.Arrays;
import javax.swing.JFrame;
import javax.swing.JPanel;
import org.knowm.xchart.CategoryChart;
import org.knowm.xchart.CategoryChartBuilder;
import org.knowm.xchart.PieChart;
import org.knowm.xchart.PieChartBuilder;
import org.knowm.xchart.XChartPanel;
import org.knowm.xchart.style.Styler.ChartTheme;

/**
 *
 * @author Kenia
 */
public class Barplot implements Instruccion {

    public NodoVector valores;
    public NodoVector etiquetas;
    public String titulo;
    public String tituloX;
    public String tituloY;
    public int linea, columna;

    public Barplot(NodoVector valores, NodoVector etiquetas, String titulo, String tituloX, String tituloY, int linea, int columna) {
        this.valores = valores;
        this.etiquetas = etiquetas;
        this.titulo = titulo;
        this.tituloX = tituloX;
        this.tituloY = tituloY;
        this.linea = linea;
        this.columna = columna;
    }
    

    @Override
    public Object ejecutar(Entorno e) {
        CategoryChart chart = new CategoryChartBuilder().width(800).height(600).title(titulo).xAxisTitle(tituloX).yAxisTitle(tituloY).build();

        Double[] v1 = new Double[valores.size()];
        String[] v2 = new String[etiquetas.size()];

        if (valores.size() == etiquetas.size()) {
            for (int i = 0; i < valores.size(); i++) {
                v1[i] = Double.parseDouble( valores.get(i).toString());
                v2[i] = etiquetas.get(i).toString();
            }

            chart.addSeries(titulo,Arrays.asList(v2), Arrays.asList(v1));

        } else {
            Singleton.getInstance().addError("La catidad de valores y etiquetas no es igual", linea, columna);
            return null;

        }

        JPanel panel = new XChartPanel<>(chart);
        JFrame frame = new JFrame(titulo);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.add(panel);
        frame.pack();
        frame.setVisible(true);

        return null;

    }

    @Override
    public int linea() {
        return this.linea;
    }

    @Override
    public int columna() {
        return this.columna;
    }
}
