/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.Graficas;

import ast.Singleton;
import ast.Vectores.NodoVector;
import ast.entorno.Entorno;
import ast.instrucciones.Instruccion;
import ast.matrix.NodoMatrix;
import java.util.ArrayList;
import java.util.Arrays;
import javax.swing.JFrame;
import javax.swing.JPanel;
import org.knowm.xchart.CategoryChart;
import org.knowm.xchart.CategoryChartBuilder;
import org.knowm.xchart.Histogram;
import org.knowm.xchart.XChartPanel;
import org.knowm.xchart.style.Styler.LegendPosition;

/**
 *
 * @author Kenia
 */
public class HistogramaPlot implements Instruccion {

    public NodoVector valores;
    public String tituloX;
    public String titulo;

    public int linea, columna;

    public HistogramaPlot(NodoVector valores, String tituloX, String titulo, int linea, int columna) {
        this.valores = valores;
        this.tituloX = tituloX;
        this.titulo = titulo;
        this.linea = linea;
        this.columna = columna;
    }

    @Override
    public Object ejecutar(Entorno e) {
        CategoryChart chart = new CategoryChartBuilder().width(800).height(600).title(titulo).xAxisTitle(tituloX).yAxisTitle("").build();

        // Customize Chart
        chart.getStyler().setAvailableSpaceFill(.9999);
        chart.getStyler().setOverlapped(true);

        Double[] v1 = new Double[valores.size()];
        Integer[] v2 = new Integer[valores.size()];
        ArrayList<Double> data = new ArrayList<Double>();

        for (int i = 0; i < valores.size(); i++) {
            //    data.add( Double.parseDouble(valores.get(i).toString()) );
            v1[i] = Double.parseDouble(valores.get(i).toString());
            v2[i] = i;
        }

        chart.addSeries(titulo, Arrays.asList(v2), Arrays.asList(v1));

        JPanel panel = new XChartPanel<>(chart);
        JFrame frame = new JFrame(titulo);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.add(panel);
        frame.pack();
        frame.setVisible(true);

        return null;

    }

    @Override
    public int linea() {
        return this.linea;
    }

    @Override
    public int columna() {
        return this.columna;
    }

}
