/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.Listas;

import ast.entorno.Tipo;

/**
 *
 * @author Kenia
 */
public class ObjetoLista {
    
    public Tipo tipo;
    public Object valor;

    public ObjetoLista(Tipo tipo, Object valor) {
        this.tipo = tipo;
        this.valor = valor;
    }

    
    
    public Tipo getTipo() {
        return tipo;
    }

    public void setTipo(Tipo tipo) {
        this.tipo = tipo;
    }

    public Object getValor() {
        return valor;
    }

    public void setValor(Object valor) {
        this.valor = valor;
    }


    
    
    
}
