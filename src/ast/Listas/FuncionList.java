/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.Listas;

import ast.entorno.Entorno;
import ast.entorno.Tipo;
import ast.expresion.Expresion;
import java.util.ArrayList;
import java.util.LinkedList;

/**
 *
 * @author Kenia
 */
public class FuncionList implements Expresion {

    public LinkedList<Expresion> valores;
    public int linea, columna;

    public FuncionList(LinkedList<Expresion> valores, int linea, int columna) {
        this.valores = valores;
        this.linea = linea;
        this.columna = columna;
    }

    @Override
    public Object getValorImplicito(Entorno e) {
        NodoLista lista = new NodoLista();
        Tipo tipo_exp = null;
        ObjetoLista objeto = null;

        for (Expresion exp : valores) {
            tipo_exp = exp.getTipo(e);

            if (null != tipo_exp) {
                objeto = new ObjetoLista(tipo_exp, exp.getValorImplicito(e));

                switch (tipo_exp.rol) {
                    case LISTA:
                        lista.add(objeto);
                        break;
                    case VECTOR:
                        lista.add(objeto);
                        break;
                    case PRIMITIVO:
                        lista.add(objeto);
                        break;
                }
            }
        }
        return lista;

    }

    @Override
    public Tipo getTipo(Entorno e) {
        return new Tipo(Tipo.Rol.LISTA, Tipo.tipo.LISTA);
    }

    @Override
    public int linea() {
        return this.linea;
    }

    @Override
    public int columna() {
        return this.columna;
    }

}
