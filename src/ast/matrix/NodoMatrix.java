/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.matrix;

import ast.entorno.Tipo;
import java.util.ArrayList;

/**
 *
 * @author Kenia
 */
public class NodoMatrix {

    public Tipo tipo;
    public Object[][] matrix;
    public int nrows, ncols;

    public NodoMatrix(Tipo tipo, Object[][] matrix, int nrows, int ncols) {
        this.tipo = tipo;
        this.matrix = matrix;
        this.nrows = nrows;
        this.ncols = ncols;
    }

    public NodoMatrix() {
    }

    public int getNrows() {
        return nrows;
    }

    public void setNrows(int nrows) {
        this.nrows = nrows;
    }

    public int getNcols() {
        return ncols;
    }

    public void setNcols(int ncols) {
        this.ncols = ncols;
    }
    
    


    public Tipo getTipo() {
        return tipo;
    }

    public void setTipo(Tipo tipo) {
        this.tipo = tipo;
    }

    public Object[][] getMatrix() {
        return matrix;
    }

    public void setMatrix(Object[][] matrix) {
        this.matrix = matrix;
    }

}
