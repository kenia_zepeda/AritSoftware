/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.matrix;

import ast.Singleton;
import ast.Vectores.NodoVector;
import ast.entorno.Entorno;
import ast.entorno.Tipo;
import ast.expresion.Expresion;
import java.util.LinkedList;

/**
 *
 * @author Kenia
 */
public class FuncionMatrix implements Expresion {

    public NodoVector data;
    public int nrows;
    public int ncols;

    public Tipo tipo;
    public LinkedList<Expresion> valores;

    public int linea, columna;

    public FuncionMatrix(NodoVector data, int nrow, int ncol, int linea, int columna) {
        this.data = data;
        this.nrows = nrow;
        this.ncols = ncol;
        this.linea = linea;
        this.columna = columna;
    }

    @Override
    public Object getValorImplicito(Entorno e) {
        Object[][] matrix = new Object[nrows][ncols];

        int tamañoMatriz = nrows * ncols;
        int col = 0;
        int lin = 0;
        int pos = 0;
        for (int i = 0; i < tamañoMatriz; i++) {
            Object obj = data.get(pos);
            matrix[lin][col] = obj;
            lin++;
            pos++;
            if (pos == data.size()) {
                pos = 0;
            }
            if (lin == nrows) {
                lin = 0;
                col++;
            }
        }
        tipo =  new Tipo(Tipo.Rol.MATRIZ, data.tipo.tp);
        NodoMatrix nodoMatrix = new NodoMatrix(tipo, matrix, nrows, ncols);
        return nodoMatrix;
    }

    @Override
    public Tipo getTipo(Entorno e) {
        Tipo tipoData = data.getTipo();
        return new Tipo(Tipo.Rol.MATRIZ, tipoData.tp);
    }

    @Override
    public int linea() {
        return this.linea;
    }

    @Override
    public int columna() {
        return this.columna;
    }

}
