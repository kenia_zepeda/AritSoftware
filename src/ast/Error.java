/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast;



/**
 *
 * @author Kenia
 */
public class Error implements NodoAst{

    public String descripcion;
    public String clase;
    public int linea;
    public int columna;

    public Error(String descripcion, int linea, int columna) {
        this.descripcion = descripcion;
        this.linea = linea;
        this.columna = columna;
    }

    public Error(String descripcion) {
        this.descripcion = descripcion;
        this.linea = 0;
        this.linea = 0;
    }
    
    
    
    

    @Override
    public int linea() {
        return this.linea;
    }

    @Override
    public int columna() {
        return this.columna;
    }

}
