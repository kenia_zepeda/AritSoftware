package ast;

import ast.entorno.Entorno;
import ast.expresion.Expresion;
import ast.instrucciones.Instruccion;
import java.util.*;

public class Ast {

    private LinkedList<NodoAst> nodos;
    public Entorno entornoGlobal;

    public Ast(LinkedList<NodoAst> nodos) {
        this.nodos = nodos;
    }

    public Object ejecutar() {
        Entorno entornoGlobal = new Entorno();

        for (NodoAst nodo : nodos) {
            if (nodo instanceof Instruccion) {
                Object result = ((Instruccion) nodo).ejecutar(entornoGlobal);
                if (result != null) {
                    return result;
                }
            } else if (nodo instanceof Expresion) {
                Object result = ((Expresion) nodo).getValorImplicito(entornoGlobal);
                if (result != null) {
                    return result;
                }
            }
        }
        return null;

    }

}
