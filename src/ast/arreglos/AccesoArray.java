/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.arreglos;

import ast.Singleton;
import ast.entorno.Entorno;
import ast.entorno.Simbolo;
import ast.entorno.Tipo;
import ast.expresion.Expresion;
import java.io.Serializable;
import java.util.LinkedList;

/**
 *
 * @author Kenia
 */
public class AccesoArray implements Expresion, Serializable {

    public String id;
    public Tipo tipo;
    public LinkedList<Expresion> dimsExps = new LinkedList<Expresion>();
    public LinkedList<Integer> dims = new LinkedList<Integer>();
    int linea, columna;

    public AccesoArray(String id, LinkedList<Expresion> dimsExps, int linea, int columna) {
        this.id = id;
        this.dimsExps = dimsExps;
        this.linea = linea;
        this.columna = columna;
    }

    @Override
    public Object getValorImplicito(Entorno e) {

        Simbolo sim = e.getSimbolo(id, linea, columna);
        if (sim != null) {

            if (sim.dim != dimsExps.size()) {
                Singleton.getInstance().addError("índice de arreglo fuera de límites.", linea, columna);
                return null;
            }

            for (Expresion num : dimsExps) {
                Object valor = num.getValorImplicito(e);
                if (valor instanceof Integer) {
                    dims.add((int) valor);
                } else {
                    Singleton.getInstance().addError("La dimensión del array no es de tipo entero", linea, columna);
                    return null;
                }
            }

            //if (sim.rol == Rol.ARREGLO) {
            NodoArray raiz = (NodoArray) sim.valor;
            return recorrerNiveles(raiz.hijos.get(0), 0, e);
            /*} else {
                Singleton.getInstance().addError("La variable " + id + " no es un arreglo.", linea, columna);
                return null;
            }*/

        }
        return null;
    }

    public Object recorrerNiveles(NodoArray raiz, int indiceActual, Entorno ent) {

        for (indiceActual = 0; indiceActual < dims.size(); indiceActual++) {
            int pos = dims.get(indiceActual);

            if (indiceActual == dims.size() - 1) {
                if (pos < raiz.hijos.size()) {
                    Object valor = raiz.hijos.get(pos).expresion.getValorImplicito(ent);
                    if (valor.toString().equals("LLAVES")) {
                        Singleton.getInstance().addError("índice de arreglo incorrecto", linea, columna);
                    } else {
                        return valor;
                    }
                } else {
                    Singleton.getInstance().addError("índice de arreglo fuera de límites.", linea, columna);
                    return null;
                }

            } else {
                if (pos < raiz.hijos.size()) {
                    raiz = raiz.hijos.get(pos);
                } else {
                    Singleton.getInstance().addError("índice de arreglo fuera de límites.", linea, columna);
                    return null;
                }
            }
        }

        /*  int pos = dims.get(indiceActual);

        if (indiceActual == dims.size() - 1) {
            if (pos <= raiz.hijos.size()) {
                Object valor = raiz.hijos.get(pos).expresion.getValorImplicito(ent);
                if (valor.toString().equals("LLAVES")) {
                    Singleton.getInstance().addError("índice de arreglo incorrecto",linea, columna);
                } else {
                    return valor;
                }
            } else {
                Singleton.getInstance().addError("índice de arreglo fuera de límites.",linea, columna);
                return null;
            }

        } else if (indiceActual < dims.size()) {
            if (pos <= raiz.hijos.size()) {
                recorrerNivel(raiz.hijos.get(pos), indiceActual + 1, ent);
            } else {
                Singleton.getInstance().addError("índice de arreglo fuera de límites.",linea, columna);
                return null;
            }
        }*/
        return null;
    }

    @Override
    public Tipo getTipo(Entorno e) {
        Simbolo sim = e.getSimbolo(id, linea, columna);
        return sim.tipo;

    }

    @Override
    public int linea() {
        return this.linea;
    }

    @Override
    public int columna() {
        return this.columna;
    }

}
