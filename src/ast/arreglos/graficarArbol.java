/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.arreglos;

import ast.entorno.Entorno;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author id_ma
 */
public class graficarArbol {

    int contador = 0;
    String conexion = "";
    public String graphText = "";
    Entorno e;

    public void grafico(NodoArray raiz, Entorno ent) {
        this.e = ent;
        graphText += "\ndigraph G {\r\nnode [shape=record, style=filled, color=pink, fontcolor=black];\n";
        crearNodos(raiz);
        conectarNodos(raiz);
        graphText += "}";

        FileWriter archivo = null;
        @SuppressWarnings("UnusedAssignment")
        PrintWriter printwriter = null;
        try {
            archivo = new FileWriter("C:\\Users\\Kenia\\Documents\\COMPI2\\junio2019\\ProyectoVacas1\\xml.dot");
            printwriter = new PrintWriter(archivo);
            printwriter.println(graphText);
            printwriter.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        try {
            String dotPath = "C:\\Program Files (x86)\\Graphviz2.38\\bin\\dot.exe";
            String fileInputPath = "C:\\Users\\Kenia\\Documents\\COMPI2\\junio2019\\ProyectoVacas1\\xml.dot";
            String fileOutputPath = "C:\\Users\\Kenia\\Documents\\COMPI2\\junio2019\\ProyectoVacas1\\xml.jpg";

            String tParam = "-Tjpg";
            String tOParam = "-o";

            String[] cmd = new String[5];
            cmd[0] = dotPath;
            cmd[1] = tParam;
            cmd[2] = fileInputPath;
            cmd[3] = tOParam;
            cmd[4] = fileOutputPath;
            
        //    System.out.println("graficar: "+ Arrays.toString(cmd));

            Runtime rt = Runtime.getRuntime();

            rt.exec(cmd);

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
        }

    }

    public void conectarNodos(NodoArray praiz) {

        if (praiz != null) {

            for (NodoArray temp : praiz.hijos) {

                if (temp != null) {
                    conexion = "\"node" + praiz.contador + "\"->";
                    conexion += "\"node" + temp.contador + "\";";
                    graphText += conexion + "\n";
                    conectarNodos(temp);
                } else {
                    NodoArray nodo = new NodoArray("nulo");
                    int cont = praiz.contador + 1;
                    conexion = "\"node" + praiz.contador + "\"->";
                    conexion += "\"node" + cont + "\";";
                    graphText += conexion + "\n";
                    conectarNodos(nodo);
                }
            }

        } else {
            NodoArray nodo = new NodoArray("nulo");
            int cont = praiz.contador + 1;
            conexion = "\"node" + praiz.contador + "\"->";
            conexion += "\"node" + cont + "\";";
            graphText += conexion + "\n";
            conectarNodos(nodo);
        }
    }

    public void crearNodos(NodoArray praiz) {
        graphText += "node" + contador + "[label=\"" + praiz.getValorExpresion(e).toString().replace("\"", "\\\"").replace(">", "\\>").replace("<", "\\<") + "\"];\n";

        if (praiz != null) {
            praiz.contador = contador;
            contador++;
            for (NodoArray temp : praiz.hijos) {
                if (temp != null) {
                    crearNodos(temp);
                } else {
                    NodoArray nodo = new NodoArray("nulo");
                    crearNodos(nodo);
                }
            }

        } else {
            NodoArray nodo = new NodoArray("nulo");
            nodo.contador = contador;
            contador++;
            crearNodos(nodo);
        }
    }

}
