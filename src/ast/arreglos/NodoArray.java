/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.arreglos;

import ast.Singleton;
import ast.entorno.Entorno;
import ast.entorno.Tipo;
import ast.expresion.Expresion;
import ast.expresion.Primitivo;
import java.io.Serializable;
import java.util.LinkedList;

/**
 *
 * @author Kenia
 */
public class NodoArray implements Expresion, Serializable {

    //Atributos para declaracion con llaves
    public Expresion expresion;
    public LinkedList<NodoArray> hijos = new LinkedList<>();

    //Atributos para declaracion con corchetes
    public LinkedList<Expresion> dimsExps = new LinkedList<>();
    public LinkedList<Integer> dims = new LinkedList<>();
    public Tipo tipo;

    public int contador; //este atributo sirve para graficar el árbol
    int linea, columna;

    public NodoArray(Expresion valor, int linea, int columna) {
        this.expresion = valor;
        this.linea = linea;
        this.columna = columna;
    }

    public NodoArray(String produccion, int linea, int columna) {
        expresion = new Primitivo(new Tipo(Tipo.tipo.STRING),produccion);
        this.linea = linea;
        this.columna = columna;
    }

    public NodoArray(Object produccion, int linea, int columna) {
        expresion = new Primitivo(tipo,produccion);
        this.linea = linea;
        this.columna = columna;
    }

    public NodoArray(String produccion, Tipo tipo, LinkedList<Expresion> dimsExps, int linea, int columna) {
        expresion = new Primitivo(new Tipo(Tipo.tipo.STRING),produccion);
        this.dimsExps = dimsExps;
        this.tipo = tipo;
        this.linea = linea;
        this.columna = columna;
    }

    NodoArray(String produccion) {
        expresion = new Primitivo(new Tipo(Tipo.tipo.STRING),produccion);
    }

    @Override
    public Object getValorImplicito(Entorno ent) {
        if (expresion.getValorImplicito(ent).toString().equals("CORCHETES")) {
            return inicializarCorchetes(ent);
        } else if (expresion.getValorImplicito(ent).toString().equals("RAIZ")) {
            validarLLaves(this, ent);
            return this;
        }
        return null;
    }

    public Object getValorExpresion(Entorno e) {
        if (expresion != null) {
            return expresion.getValorImplicito(e);
        }
        return 0;
    }

    public Object inicializarCorchetes(Entorno ent) {

        for (Expresion num : dimsExps) {
            Object valor = num.getValorImplicito(ent);
            if (valor instanceof Integer) {
                dims.add((int) valor);
            } else {
                Singleton.getInstance().addError("La dimensión del array no es de tipo entero",  linea, columna);
                return null;
            }
        }
        NodoArray raiz = new NodoArray("RAIZ", linea, columna);
        raiz.hijos.add(new NodoArray("LLAVES", linea, columna));
        crearNodos(0, raiz);
        return raiz;

    }

    public void crearNodos(int indiceActual, NodoArray raiz) {
        if (indiceActual == dims.size() - 1) {

            for (NodoArray hijo : raiz.hijos) {
                for (int i = 0; i < dims.get(indiceActual); i++) {
                    hijo.hijos.add(new NodoArray(0, linea, columna));
                }
                crearNodos(indiceActual + 1, hijo);
            }
        } else if (indiceActual < dims.size()) {
            for (NodoArray hijo : raiz.hijos) {
                for (int i = 0; i < dims.get(indiceActual); i++) {
                    hijo.hijos.add(new NodoArray("LLAVES", linea, columna));
                }
                crearNodos(indiceActual + 1, hijo);
            }

        }
    }

    public void validarLLaves(NodoArray raiz, Entorno ent) {
        for (NodoArray hijo : raiz.hijos) {
            Object valor = hijo.expresion.getValorImplicito(ent);
            if (!valor.toString().equals("LLAVES") && !valor.toString().equals("RAIZ")) {
                Tipo.tipo tip = hijo.expresion.getTipo(ent).tp;
                if (tip == tipo.tp) {
                    hijo.expresion = new Primitivo(tipo,valor);
                } else {
                    Singleton.getInstance().addError("El valor no coincide con el tipo de dato del array",  linea, columna);
                }
                validarLLaves(hijo, ent);
            } else {
                validarLLaves(hijo, ent);
            }

        }
    }

    @Override
    public Tipo  getTipo(Entorno e) {
        return tipo;
    }

    @Override
    public int linea() {
        return this.linea;
    }

    @Override
    public int columna() {
        return this.columna;
    }

}
