/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.arreglos;

import ast.Singleton;
import ast.entorno.Entorno;
import ast.entorno.Simbolo;
import ast.entorno.Tipo;
import ast.expresion.Expresion;
import ast.expresion.Primitivo;
import ast.instrucciones.Instruccion;
import java.io.Serializable;
import java.util.LinkedList;

/**
 *
 * @author Kenia
 */
public class AsignacionArray implements Instruccion, Serializable {

    String id;
    public LinkedList<Expresion> dimsExps = new LinkedList<>();
    public LinkedList<Integer> dims = new LinkedList<>();
    public Expresion nuevoValor;
    Tipo tipo;
    NodoArray raiz;

    public int linea, columna;

    public AsignacionArray(String id, LinkedList<Expresion> dimsExps, Expresion nuevoValor, int linea, int columna) {
        this.id = id;
        this.dimsExps = dimsExps;
        this.nuevoValor = nuevoValor;
        this.linea = linea;
        this.columna = columna;
    }

    @Override
    public Object ejecutar(Entorno ent) {

        if (ent.existeEnActual(id)) {

            Simbolo sim = ent.getSimbolo(id, linea, columna);
            tipo = sim.getTipo();

            if (sim.dim != dimsExps.size()) {
                Singleton.getInstance().addError("índice de arreglo fuera de límites.",  linea, columna);
                return null;
            }

            if (nuevoValor != null) {

                if (nuevoValor.getTipo(ent).tp == tipo.tp) {

                    raiz = (NodoArray) sim.valor;
                    for (Expresion num : dimsExps) {
                        Object valor = num.getValorImplicito(ent);
                        if (valor instanceof Integer) {
                            dims.add((int) valor);
                        } else {
                            Singleton.getInstance().addError("La dimensión del array no es de tipo entero",  linea, columna);
                            return null;
                        }
                    }

                    recorrerNiveles(raiz.hijos.get(0), ent);
                    ent.getSimbolo(id, linea, columna).setValor(raiz);

                } else {

                    Singleton.getInstance().addError("El tipo de dato asignado no coincide con el tipo de dato de la variable",  linea, columna);

                }
            } else {
                Singleton.getInstance().addError("El tipo de dato de la asignado no coincide con el tipo de dato de la variable",  linea, columna);
            }
        } else {
            Singleton.getInstance().addError("El símbolo: " + id + " no existe en el entorno actual",  linea, columna);
        }

        return null;
    }

    public void recorrerNiveles(NodoArray r, Entorno ent) {

        for (int indiceActual = 0; indiceActual < dims.size(); indiceActual++) {
            int pos = dims.get(indiceActual);
            if (indiceActual == dims.size() - 1) {
                if (pos < r.hijos.size()) {
                    Object valor = r.hijos.get(pos).expresion.getValorImplicito(ent);
                    if (valor.toString().equals("LLAVES")) {
                        Singleton.getInstance().addError("índice de arreglo incorrecto",  linea, columna);
                    } else {
                        r.hijos.get(pos).expresion = new Primitivo(tipo, nuevoValor.getValorImplicito(ent));
                        break;
                    }
                } else {
                    Singleton.getInstance().addError("índice de arreglo fuera de límites.",  linea, columna);
                    break;
                }

            } else {
                if (pos < r.hijos.size()) {
                    r = r.hijos.get(pos);
                } else {
                    Singleton.getInstance().addError("índice de arreglo fuera de límites.",  linea, columna);
                    break;
                }
            }
        }

    }

    @Override
    public int linea() {
        return this.linea;
    }

    @Override
    public int columna() {
        return this.columna;
    }

}
