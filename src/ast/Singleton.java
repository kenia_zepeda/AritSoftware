/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Kenia
 */
public class Singleton implements Serializable{

    //public ArrayList<String> AstErrores = new ArrayList<>();
    public ArrayList<Error> AstErrores = new ArrayList<>();
    public ArrayList<String> AstSalida = new ArrayList<>();
    Boolean banderaMain = false;
    public String idClasePrincipal = "";
    public int contador_break = 0;

    private Singleton() {
    }

    public static Singleton getInstance() {
        return SingletonHolder.INSTANCE;
    }

    private static class SingletonHolder {

        private static final Singleton INSTANCE = new Singleton();
    }

    public void addError(String descripcion, int linea, int columna) {
        AstErrores.add(new ast.Error(descripcion, linea, columna));
    }

    public void addError(String descripcion) {
        AstErrores.add(new ast.Error(descripcion));
    }

  

}
