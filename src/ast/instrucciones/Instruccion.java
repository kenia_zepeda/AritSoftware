/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.instrucciones;

import ast.NodoAst;
import ast.entorno.Entorno;

/**
 *
 * @author Kenia
 */
public interface Instruccion extends NodoAst  {

      Object ejecutar(Entorno e);

    
    
}
