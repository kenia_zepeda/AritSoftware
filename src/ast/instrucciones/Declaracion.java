/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.instrucciones;

import ast.Singleton;
import ast.entorno.Entorno;
import ast.entorno.Simbolo;
import ast.entorno.Tipo;
import ast.expresion.Expresion;

/**
 *
 * @author Kenia
 */
public class Declaracion implements Instruccion {

    Tipo tipo;
    public String id;
    public Expresion valor;
    public int dimensiones;
    int linea, columna;

    public Declaracion(String id, Expresion valor, int linea, int columna) {
        this.id = id;
        this.valor = valor;
        this.linea = linea;
        this.columna = columna;
    }

    public Declaracion(String id, int linea, int columna) {
        this.id = id;
        this.valor = null;
        this.linea = linea;
        this.columna = columna;
    }

    @Override
    public Object ejecutar(Entorno ent) {

        if (valor != null) {
            // valor.getValorImplicito(ent);
            tipo = valor.getTipo(ent);

            if (tipo != null) {

                if (tipo.rol == Tipo.Rol.PRIMITIVO || tipo.rol == Tipo.Rol.VECTOR || tipo.rol == Tipo.Rol.LISTA || tipo.rol == Tipo.Rol.MATRIZ || tipo.rol == Tipo.Rol.PARAMETRO) {
                    if (ent.existe(id)) {
                        // Singleton.getInstance().addError("Se intento declarar una variable ya existente en el entorno actual", linea, columna);
                        ent.getSimbolo(id, linea, columna).setValor(valor.getValorImplicito(ent));
                        ent.getSimbolo(id, linea, columna).setTipo(tipo);
                    } else {
                        ent.agregar(id, new Simbolo(tipo, id, valor.getValorImplicito(ent)));
                    }
                }

            } else {
                /*   if (ent.existeEnActual(id)) {
                Singleton.getInstance().addError("Se intento declarar una variable ya existente en el entorno actual", linea, columna);
            } else {
                tipo = new Tipo(Tipo.Rol.PRIMITIVO, Tipo.tipo.DESCONOCIDO);
                ent.agregar(id, new Simbolo(tipo, id, null));
            }*/

            }

        } else {
            Singleton.getInstance().addError("Error en la declaración de la variable: " + id, linea, columna);

        }
        return null;
    }

    @Override
    public int linea() {
        return this.linea;
    }

    @Override
    public int columna() {
        return this.columna;
    }

}
