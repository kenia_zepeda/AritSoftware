/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.instrucciones;

import ast.entorno.Entorno;
import java.io.Serializable;

/**
 *
 * @author Kenia
 */
public class Continue implements Instruccion {

    int linea, columna;

    public Continue(int linea, int columna) {
        this.linea = linea;
        this.columna = columna;
    }
    
    

    @Override
    public Object ejecutar(Entorno e) {
        return null;
    }

    @Override
    public int linea() {
        return this.linea;
    }

    @Override
    public int columna() {
        return this.columna;
    }

}
