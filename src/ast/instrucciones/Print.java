/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.instrucciones;

import ast.Listas.NodoLista;
import ast.Listas.ObjetoLista;
import ast.Singleton;
import ast.entorno.Entorno;
import ast.entorno.Tipo;
import ast.expresion.Expresion;
import ast.matrix.NodoMatrix;
import java.io.Serializable;

/**
 *
 * @author Kenia
 */
public class Print implements Instruccion, Serializable {

    Expresion toPrint;
    int linea;
    int columna;

    public Print(Expresion toPrint, int linea, int columna) {
        this.toPrint = toPrint;
        this.linea = linea;
        this.columna = columna;
    }

    @Override
    public Object ejecutar(Entorno e) {
        if (toPrint != null) {
            Tipo tipo_exp = toPrint.getTipo(e);
            Object valor = null;
            if (null != tipo_exp) {
                switch (tipo_exp.rol) {
                    case LISTA:
                        valor = toPrint.getValorImplicito(e);
                        NodoLista lista = new NodoLista();
                        lista = (NodoLista) valor;
                        for (ObjetoLista object : lista) {
                            if (object != null) {
                                Singleton.getInstance().AstSalida.add(object.getValor().toString());
                            }
                        }
                        break;
                    case VECTOR:
                        valor = toPrint.getValorImplicito(e);
                        if (valor != null) {
                            Singleton.getInstance().AstSalida.add(valor.toString());
                        }
                        break;
                    case MATRIZ:
                        valor = toPrint.getValorImplicito(e);
                        NodoMatrix matriz = (NodoMatrix) valor;
                        Object[][] matrix = matriz.getMatrix();
                        String salida = "";
                        for (int i = 0; i < matriz.nrows; i++) {
                            // El primer índice recorre las filas.
                            salida =  salida + "[";
                            for (int j = 0; j < matriz.ncols; j++) {	// El segundo índice recorre las columnas.
                                salida = salida + matrix[i][j].toString()+",";
                            }
                            salida = salida + "] \n";
                        }
                        Singleton.getInstance().AstSalida.add(salida);
                        break;
                    case PRIMITIVO:
                        valor = toPrint.getValorImplicito(e);
                        if (valor != null) {
                            Singleton.getInstance().AstSalida.add(valor.toString());
                        }
                        break;
                    default:
                        valor = toPrint.getValorImplicito(e);
                        if (valor != null) {
                            Singleton.getInstance().AstSalida.add(valor.toString());
                        }
                        break;
                }
            }

        }
        return null;
    }

    @Override
    public int linea() {
        return this.linea;
    }

    @Override
    public int columna() {
        return this.columna;
    }

}
