/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.instrucciones;

import ast.Singleton;
import ast.entorno.Entorno;
import ast.expresion.Expresion;
import java.io.Serializable;

/**
 *
 * @author Kenia
 */
public class Return implements Instruccion {

    public boolean retornoVoid;
    public Expresion valorDeRetorno;
    public int linea, columna;

    public Return(Expresion valorDeRetorno, int linea, int columna) {
        this.valorDeRetorno = valorDeRetorno;
        retornoVoid = false;
        this.linea = linea;
        this.columna = columna;
    }

    public Return(int linea, int columna) {
        retornoVoid = true;
        valorDeRetorno = null;
        this.linea = linea;
        this.columna = columna;
    }

    public final boolean isRetornoVoid() {
        return retornoVoid;
    }

    @Override
    public int linea() {
        return this.linea;

    }

    @Override
    public int columna() {
        return this.columna;
    }

    @Override
    public Object ejecutar(Entorno e) {
        if (valorDeRetorno == null) {
            return null;
        } else {
            Object valor = valorDeRetorno.getValorImplicito(e);
            if (valor != null) {
                return valor;
            } else {
                Singleton.getInstance().addError("El valor de retorno es nulo", linea, columna);

            }
        }
        return null;
    }

}
