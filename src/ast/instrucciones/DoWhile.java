/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.instrucciones;

import ast.NodoAst;
import ast.Singleton;
import ast.entorno.Entorno;
import ast.expresion.Expresion;
import java.io.Serializable;
import java.util.LinkedList;

/**
 *
 * @author Kenia
 */
public class DoWhile implements Instruccion, Serializable {

    LinkedList<NodoAst> instrucciones;
    Expresion condicion;
    int linea, columna;

    public DoWhile(Expresion condicion, LinkedList<NodoAst> instrucciones, int linea, int columna) {
        this.instrucciones = instrucciones;
        this.condicion = condicion;
        this.linea = linea;
        this.columna = columna;
    }

    @Override
    public Object ejecutar(Entorno ent) {
        siguiente:
        do {
            Entorno local = new Entorno(ent);
            for (NodoAst nodo : instrucciones) {

                if (nodo instanceof Continue) {
                    continue siguiente;
                }
                if (nodo instanceof Break) {
                    Singleton.getInstance().contador_break = 0;
                    return null;
                }
                if (nodo instanceof Instruccion) {
                    Instruccion ins = (Instruccion) nodo;
                    Object result = ins.ejecutar(local);
                    if (result != null) {
                        return result;
                    }
                } else if (nodo instanceof Expresion) {
                    Expresion expr = (Expresion) nodo;
                    expr.getValorImplicito(local);
                }
            }
        } while ((Boolean) condicion.getValorImplicito(ent));
        return null;
    }

    @Override
    public int linea() {
        return this.linea;
    }

    @Override
    public int columna() {
        return this.columna;
    }

}
