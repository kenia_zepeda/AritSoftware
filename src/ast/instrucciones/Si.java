/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.instrucciones;

import ast.NodoAst;
import ast.Singleton;
import ast.entorno.Entorno;
import ast.expresion.Expresion;
import java.io.Serializable;
import java.util.LinkedList;

/**
 *
 * @author Kenia
 */
public class Si implements Instruccion, Serializable {

    private LinkedList<NodoAst> instrucciones;
    private Expresion condicion;
    int linea, columna;

    public Si(LinkedList<NodoAst> instrucciones, Expresion condicion, int linea, int columna) {
        this.instrucciones = instrucciones;
        this.condicion = condicion;
        this.linea = linea;
        this.columna = columna;
    }



    @Override
    public Object ejecutar(Entorno ent) {
        
        boolean op = (boolean) (condicion.getValorImplicito(ent));
        if (op) {
            Entorno local = new Entorno(ent);
            for (NodoAst nodo : instrucciones) {
                if (nodo instanceof Continue) {
                    continue;
                }

                if (nodo instanceof Break) {
                    Singleton.getInstance().contador_break = 0;
                    return nodo;
                }
                if (nodo instanceof Instruccion) {
                    Instruccion ins = (Instruccion) nodo;
                    Object result = ins.ejecutar(local);
                    if (result != null) {
                        return result;
                    }
                } else if (nodo instanceof Expresion) {
                    Expresion expr = (Expresion) nodo;
                    return expr.getValorImplicito(local);
                }
            }
        }
        return null;
    }

    @Override
    public int linea() {
        return this.linea;
    }

    @Override
    public int columna() {
        return this.columna;
    }

}
