/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.instrucciones;

import ast.Singleton;
import ast.entorno.Entorno;
import ast.entorno.Simbolo;
import java.io.Serializable;
import java.util.Set;

/**
 *
 * @author Kenia
 */
public class PrintTabla implements Instruccion, Serializable {

    int i = 0;
    int linea, columna;

    @Override
    public Object ejecutar(Entorno e) {

        int i = 0;
        print(e);
        /*   for (Entorno ent = e; ent != null; ent = ent.padre) {

            Set<String> keys = ent.tabla.keySet();

            for (String key : keys) {
                Simbolo s = (Simbolo) ent.tabla.get(key);
                Singleton.getInstance().AstSalida.add(" Entorno " + i + " --> ID: " + s.id + " | VALOR: " + s.getValor() + " | \n");
            }

            i++;
        }*/
        return null;

    }

    public void print(Entorno ent) {

        if (ent != null) {

            Set<String> keys = ent.tabla.keySet();

            for (String key : keys) {
                Simbolo s = ent.tabla.get(key);
                Singleton.getInstance().AstSalida.add(" Entorno " + i + " --> Tipo: " + s.tipo.rol + " " + s.tipo.tp + " | ID: " + s.id + " | VALOR: " + s.getValor() + " | \n");
            }
            i++;
            print(ent.padre);

        }

    }

    @Override
    public int linea() {
        return this.linea;
    }

    @Override
    public int columna() {
        return this.columna;
    }

}
