/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.instrucciones;

import ast.Singleton;
import ast.entorno.Entorno;
import ast.expresion.Expresion;
import ast.operaciones.Operacion.Operador;
import ast.operaciones.Relacional;
import java.io.Serializable;
import java.util.LinkedList;

/**
 *
 * @author Kenia
 */
public class Switch implements Instruccion, Serializable {

    public Expresion op1;
    public LinkedList<Case> casos;
    Case defecto;
    int linea, columna;

    public Switch(Expresion valor, LinkedList<Case> casos, int linea, int columna) {
        this.op1 = valor;
        this.casos = casos;
        this.linea = linea;
        this.columna = columna;
        this.defecto = null;
    }

    public Switch(Expresion op1, LinkedList<Case> casos, Case defecto, int linea, int columna) {
        this.op1 = op1;
        this.casos = casos;
        this.defecto = defecto;
        this.linea = linea;
        this.columna = columna;
    }

    @Override
    public Object ejecutar(Entorno ent) {
        Entorno local = new Entorno(ent);
        Object result;
        boolean condicion = false;
        for (Case caso : casos) {

            Expresion condCase = new Relacional(op1, caso.op2, Operador.IGUAL_IGUAL, linea, columna);
            condicion = (Boolean) condCase.getValorImplicito(ent);

            if (condicion) {
                result = caso.ejecutar(local);
                if (result instanceof Break) {
                    Singleton.getInstance().contador_break = 0;
                    return null;
                }
                return result;
            }

        }

        if (!condicion) {

            if (defecto != null) {
               result =  defecto.ejecutar(local);
               return result;
            }

        }

        return null;
    }

    @Override
    public int linea() {
        return this.linea;
    }

    @Override
    public int columna() {
        return this.columna;
    }

}
