/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.instrucciones;

import ast.Listas.NodoLista;
import ast.Listas.ObjetoLista;
import ast.NodoAst;
import ast.Singleton;
import ast.entorno.Entorno;
import ast.entorno.Simbolo;
import ast.entorno.Tipo;
import ast.expresion.Expresion;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;

/**
 *
 * @author Kenia
 */
public class For implements Instruccion, Serializable {

    public Declaracion inicializador;
    public Expresion condicion;
    public LinkedList<NodoAst> instrucciones;
    public String idVariable;
    public Expresion vector;

    public int linea, columna;



    public For(String idVariable, Expresion vector, LinkedList<NodoAst> instrucciones, int linea, int columna) {
        this.instrucciones = instrucciones;
        this.idVariable = idVariable;
        this.vector = vector;
        this.linea = linea;
        this.columna = columna;
    }

    @Override
    public Object ejecutar(Entorno ent) {
        // inicializador.ejecutar(ent);

        Tipo tipoExp = vector.getTipo(ent);

        if (tipoExp.rol == Tipo.Rol.VECTOR) {
            ArrayList<Object> vec = (ArrayList<Object>) vector.getValorImplicito(ent);
            Siguiente:
            for (Object obj : vec) {
                Entorno local = new Entorno(ent);
                local.agregar(idVariable, new Simbolo(tipoExp, idVariable, obj));

                for (NodoAst nodo : instrucciones) {
                    if (nodo instanceof Continue) {
                        continue Siguiente;
                    }

                    if (nodo instanceof Break) {
                        Singleton.getInstance().contador_break = 0;
                        return nodo;
                    }
                    if (nodo instanceof Instruccion) {
                        Instruccion ins = (Instruccion) nodo;
                        Object result = ins.ejecutar(local);
                        if (result != null) {
                            return result;
                        }
                    } else if (nodo instanceof Expresion) {
                        Expresion expr = (Expresion) nodo;
                        return expr.getValorImplicito(local);
                    }
                }
            }

        } else if (tipoExp.rol == Tipo.Rol.LISTA) {
            ArrayList<ObjetoLista> vec = (ArrayList<ObjetoLista>) vector.getValorImplicito(ent);
            Siguiente:
            for (ObjetoLista obj : vec) {
                Entorno local = new Entorno(ent);
                local.agregar(idVariable, new Simbolo(obj.getTipo(), idVariable, obj.getValor()));

                for (NodoAst nodo : instrucciones) {
                    if (nodo instanceof Continue) {
                        continue Siguiente;
                    }

                    if (nodo instanceof Break) {
                        Singleton.getInstance().contador_break = 0;
                        return nodo;
                    }
                    if (nodo instanceof Instruccion) {
                        Instruccion ins = (Instruccion) nodo;
                        Object result = ins.ejecutar(local);
                        if (result != null) {
                            return result;
                        }
                    } else if (nodo instanceof Expresion) {
                        Expresion expr = (Expresion) nodo;
                        return expr.getValorImplicito(local);
                    }
                }
            }

        } else if (tipoExp.rol == Tipo.Rol.PRIMITIVO) {

            Entorno local = new Entorno(ent);
            local.agregar(idVariable, new Simbolo(tipoExp, idVariable, vector.getValorImplicito(ent)));

            for (NodoAst nodo : instrucciones) {
                if (nodo instanceof Continue) {
                    // continue Siguiente;
                }

                if (nodo instanceof Break) {
                    Singleton.getInstance().contador_break = 0;
                    return nodo;
                }
                if (nodo instanceof Instruccion) {
                    Instruccion ins = (Instruccion) nodo;
                    Object result = ins.ejecutar(local);
                    if (result != null) {
                        return result;
                    }
                } else if (nodo instanceof Expresion) {
                    Expresion expr = (Expresion) nodo;
                    return expr.getValorImplicito(local);
                }
            }

        }

        /*     Siguiente:
        while ((Boolean) condicion.getValorImplicito(ent)) {

            Entorno local = new Entorno(ent);
            for (NodoAst nodo : instrucciones) {
                if (nodo instanceof Continue) {
                    continue Siguiente;
                }
                
                if (nodo instanceof Break) {
                    Singleton.getInstance().contador_break = 0;
                    return nodo;
                }
                if (nodo instanceof Instruccion) {
                    Instruccion ins = (Instruccion) nodo;
                    Object result = ins.ejecutar(local);
                    if (result != null) {
                        return result;
                    }
                } else if (nodo instanceof Expresion) {
                    Expresion expr = (Expresion) nodo;
                    return expr.getValorImplicito(local);
                }
            }
            incrementador.getValorImplicito(ent);
        }*/
        return null;
    }

    @Override
    public int linea() {
        return this.linea;
    }

    @Override
    public int columna() {
        return this.columna;
    }

}
