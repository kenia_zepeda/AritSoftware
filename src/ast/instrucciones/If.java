/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.instrucciones;

/**
 *
 * @author Kenia
 */
import ast.NodoAst;
import ast.entorno.Entorno;
import java.io.Serializable;
import java.util.LinkedList;

public class If implements Instruccion, Serializable {

    private final LinkedList<NodoAst> subIfs;
    int linea, columna;

    public If(SubIf a, int linea, int columna) {
        subIfs = new LinkedList<>();
        subIfs.add(a);
        this.linea = linea;
        this.columna = columna;
    }

    public If(SubIf a, LinkedList<SubIf> b, int linea, int columna) {
        subIfs = new LinkedList<>();
        subIfs.add(a);
        subIfs.addAll(b);
        this.linea = linea;
        this.columna = columna;
    }

    public If(SubIf a, LinkedList<SubIf> b, SubIf c, int linea, int columna) {
        subIfs = new LinkedList<>();
        subIfs.add(a);
        subIfs.addAll(b);
        subIfs.add(c);
        this.linea = linea;
        this.columna = columna;
    }

    public If(SubIf a, SubIf b, int linea, int columna) {
        subIfs = new LinkedList<>();
        subIfs.add(a);
        subIfs.add(b);
        this.linea = linea;
        this.columna = columna;
    }

    @Override
    public Object ejecutar(Entorno e) {
        Object result;
        for (NodoAst nodo : subIfs) {
            if (nodo instanceof Instruccion) {
                Instruccion ins = (Instruccion) nodo;
                result = ins.ejecutar(e);
                boolean condicion = ((SubIf) ins).getValorCondicion();
                
                if (condicion) {
                    return result;
                }
                               
            }
        }
        return null;
    }

    @Override
    public int linea() {
        return this.linea;
    }

    @Override
    public int columna() {
        return this.columna;
    }
}
