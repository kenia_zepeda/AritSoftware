package ast.instrucciones;

import ast.NodoAst;
import ast.Singleton;
import ast.entorno.Entorno;
import ast.expresion.Expresion;
import java.io.Serializable;
import java.util.LinkedList;

public class SubIf implements Instruccion, Serializable {

    private Boolean valorCondicion;
    int linea, columna;

    private final boolean isElse;

    private final Expresion condicion;

    private final LinkedList<NodoAst> listaInstrucciones;

    public SubIf(Expresion a, LinkedList<NodoAst> b, int linea, int columna) {
        condicion = a;
        listaInstrucciones = b;
        isElse = false;
        this.linea = linea;
        this.columna = columna;
    }

    public SubIf(LinkedList<NodoAst> a, int linea, int columna) {
        condicion = null;
        listaInstrucciones = a;
        isElse = true;
        this.linea = linea;
        this.columna = columna;
    }

    public Boolean getValorCondicion() {
        return valorCondicion || isElse;
    }

    @Override
    public Object ejecutar(Entorno ts) {
        if (condicion == null) {
            
            valorCondicion = false;
        } else {
            Object val = condicion.getValorImplicito(ts);
            if (val != null) {
                valorCondicion = (Boolean) val;
            } else {
                    return null;
            }
        }

        if (valorCondicion || isElse) {
            Entorno local = new Entorno(ts);
            //   tablaLocal.addAll(ts);
            for (NodoAst nodo : listaInstrucciones) {
                if (nodo instanceof Continue) {
                    return nodo;
                }
                if (nodo instanceof Break) {
                    Singleton.getInstance().contador_break = 0;
                    return nodo;
                }
                if (nodo instanceof Instruccion) {
                    Instruccion ins = (Instruccion) nodo;
                    Object result = ins.ejecutar(local);
                    if (result != null) {
                        return result;
                    }
                } else if (nodo instanceof Expresion) {
                    Expresion expr = (Expresion) nodo;
                    Object result = expr.getValorImplicito(local);
                    //if (result != null) {
                    //  return result;
                    // }
                }
            }
        }
        return null;
    }

    @Override
    public int linea() {
        return this.linea;
    }

    @Override
    public int columna() {
        return this.columna;
    }

}
