/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ast.instrucciones;

import ast.NodoAst;
import ast.Singleton;
import ast.entorno.Entorno;
import ast.expresion.Expresion;
import java.io.Serializable;
import java.util.LinkedList;

/**
 *
 * @author Kenia
 */
public class Case implements Instruccion, Serializable {

    Expresion op2;
    LinkedList<NodoAst> instrucciones;
    int linea,columna;
   

    public Case(Expresion condicion, LinkedList<NodoAst> instrucciones, int fila, int columna) {
        this.op2 = condicion;
        this.instrucciones = instrucciones;
        this.linea = fila;
        this.columna = columna;
    }

    public Case(LinkedList<NodoAst> instrucciones, int fila, int columna) {
        this.instrucciones = instrucciones;
        this.linea = fila;
        this.columna = columna;
    }
    
    

    @Override
    public Object ejecutar(Entorno ent) {
        for (NodoAst nodo : instrucciones) {
            if (nodo instanceof Continue) {
                continue;
            }

            if (nodo instanceof Break) {
                Singleton.getInstance().contador_break = 0;
                return nodo;
            }
            if (nodo instanceof Instruccion) {
                Instruccion ins = (Instruccion) nodo;
                Object result = ins.ejecutar(ent);
                if (result != null) {
                    return result;
                }
            } else if (nodo instanceof Expresion) {
                Expresion expr = (Expresion) nodo;
                return expr.getValorImplicito(ent);
            }
        }
        return null;
    }

    @Override
    public int linea() {
        return this.linea;
    }

    @Override
    public int columna() {
        return this.columna;
    }

}
